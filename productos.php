<?php
require_once "dist/libs/conexion.php";
$ls_categorias = '';

$categorias = $db
  ->ObjectBuilder()
  ->rawQuery('SELECT *  FROM categorias WHERE estado_c = 1 ORDER BY  CAST(posicion_c as SIGNED INTEGER) ASC ');

foreach ($categorias as $categoria) {
  $ls_categorias .= '<li id="C-' . $categoria->Id_c . '"> <a href="#!">' . $categoria->nombre_c . '</a> </li>';
}


?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords" lang="es" content="">
  <meta name="robots" content="All">
  <meta name="description" lang="es" content="">
  <title>Productos | CEMATCOL | Cementos y Materiales de Colombia</title>
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" href="dist/css/load.css" />
  <link rel="stylesheet" href="dist/css/bundled.css" />
  <link rel="stylesheet" href="dist/css/jquery-confirm.min.css" />
  <link rel="stylesheet" href="dist/css/easy-autocomplete.css">
  <link rel="stylesheet" href="dist/css/material-icons.css" />
  <style>
    .jconfirm.jconfirm-white .jconfirm-box,
    .jconfirm.jconfirm-light .jconfirm-box {
      max-width: 800px !important;
    }
  </style>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-QHGYVVW2HJ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-QHGYVVW2HJ');
  </script>
</head>

<body>
  <header>
    <?php include("dist/libs/pages/header-top.php") ?>
  </header>

  <section>
    <div class="Mashead">
      <div class="Mashead-int">
        <div class="Mashead-int-sombra">
          <div class="Mashead-int-texto">
            <div class="Mashead-int-texto-con">
              <h1>PRODUCTOS</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="Seccion-prodmarcas">
      <div class="Seccion-prodmarcas-int">
        <div class="Seccion-filtro">
          <div class="Seccion-filtro-int">
            <a href="#!" class="" id="Fil-marca"><i class="icon-equalizer"></i> Filtrar</a>
          </div>
        </div>
        <div class="Seccion-prodmarcas-int-contenedor">
          <div class="Seccion-prodmarcas-int-contenedor-sec1 Ocultar-filtro">
            <div class="Filtro">
              <div class="Filtro-int">
                <ul class="Categorias-productos">
                  <?php echo $ls_categorias ?>
                </ul>
              </div>
            </div>
          </div>
          <div class="Seccion-prodmarcas-int-contenedor-sec2">
            <div class="listado-productos"></div>
            <div class="listado-loader">
              <div class="preloader-wrapper big active">
                <div class="spinner-layer">
                  <div class="circle-clipper left">
                    <div class="circle"></div>
                  </div>
                  <div class="gap-patch">
                    <div class="circle"></div>
                  </div>
                  <div class="circle-clipper right">
                    <div class="circle"></div>
                  </div>
                </div>
              </div>
            </div>
            <br>
            <div class="listado-paginacion">
              <ul class="pagination">
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section>
    <?php include("dist/libs/pages/marcas-aliadas.php") ?>
  </section>

  <section>
    <?php include("dist/libs/pages/contacto.php") ?>
  </section>
  <section>
    <?php include("dist/libs/pages/whatsapp.php") ?>
  </section>

  <script src="dist/js/jquery.min.js"></script>
  <script src="dist/js/menu-public.js"></script>
  <script src="dist/js/TweenMax.min.js"></script>
  <script src="dist/js/aos.js"></script>
  <script src="dist/js/jquery.superslides.min.js"></script>
  <script src="dist/js/mapa-contacto-google.js"></script>
  <script src="dist/js/smooth-scroll.polyfills.js"></script>
  <script src="dist/js/jquery-confirm.min.js"></script>
  <script src="dist/js/jquery.easy-autocomplete.min.js"></script>
  <script src="dist/js/listados-productos.js?v<?php echo date('YmdHis') ?>"></script>
  <script>
    var scroll = new SmoothScroll('a[href*="#"]', {

      // Selectors
      ignore: '[data-scroll-ignore]', // Selector for links to ignore (must be a valid CSS selector)
      header: null, // Selector for fixed headers (must be a valid CSS selector)
      topOnEmptyHash: true, // Scroll to the top of the page for links with href="#"

      // Speed & Duration
      speed: 500, // Integer. Amount of time in milliseconds it should take to scroll 1000px
      speedAsDuration: false, // If true, use speed as the total duration of the scroll animation
      durationMax: null, // Integer. The maximum amount of time the scroll animation should take
      durationMin: null, // Integer. The minimum amount of time the scroll animation should take
      clip: true, // If true, adjust scroll distance to prevent abrupt stops near the bottom of the page
      offset: 160,


      easing: 'easeInOutCubic', // Easing pattern to use
      customEasing: function(time) {


        return time < 0.5 ? 2 * time * time : -1 + (4 - 2 * time) * time;

      },

      // History
      updateURL: true, // Update the URL on scroll
      popstate: true, // Animate scrolling with the forward/backward browser buttons (requires updateURL to be true)

      // Custom Events
      emitEvents: true // Emit custom events

    });
  </script>
  <script>
    $(document).ready(function() {
      $('#Fil-marca').on('click', function() {
        console.log('Click');
        $('.Seccion-prodmarcas-int-contenedor-sec1').toggleClass('Ocultar-filtro')

      });
    });
  </script>
  <script>
    AOS.init();
    $(function() {
      $('#slides').superslides({
        inherit_width_from: '.wide-container',
        inherit_height_from: '.wide-container',
        play: 3000,
        animation: 'fade'
      });
    });
  </script>


  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfdouGws62iXdkOXVek4RmrdTQSuOJs38&callback=initMap" type="text/javascript"></script>
</body>

</html>
