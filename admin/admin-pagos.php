<?php
session_start();
if (!isset($_SESSION['cematcol_user'])) {
  header('Location: login');
}
require_once "../dist/libs/conexion.php";

?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords" lang="es" content="">
  <meta name="robots" content="All">
  <meta name="description" lang="es" content="">
  <title>Admin | CEMATCOL | Cementos y Materiales de Colombia</title>
  <link rel="stylesheet" href="../dist/css/material-icons.css" />
  <link rel="stylesheet" href="../dist/css/materialize.css" />
  <link rel="stylesheet" href="../dist/css/load.css" />
  <link rel="stylesheet" href="../dist/css/bundled.css" />
  <link rel="stylesheet" href="../dist/css/jquery-confirm.min.css" />
  <link rel="stylesheet" href="../dist/css/jquery.modal.css" />
  <link rel="stylesheet" href="../dist/js/croppie/croppie.css" />
  <link rel="stylesheet" href="../dist/css/administrador.css" />
  <style>
    #upload-demo {
      width: 100%;
      height: 400px;
    }

    .select-dropdown {
      max-height: 300px;
    }
  </style>
</head>

<body>
  <nav>
    <?php include("header-admin.php") ?>
  </nav>
  <div class="Contenedor-admin-global">
    <div class="Contenedor-admin-global-int">
      <div class="Contenedor-admin-titulo">
        <h4 class="Titulh4">Pagos</h4>
      </div>

      <div class="Contenedor-admin-texto">

      </div>

      <!-- <div class="Contenedor-admin-texto">
        <div class="Conten-completo">
          <div class="Conten-cuatro">
            <div class="input-field">
              <input id="buscar-nombre" type="text" class="validate">
              <label for="buscar-nombre">Buscar producto</label>
            </div>
          </div>
          <div class="Conten-cuatro Margin-top">
            <button class="btn light-blue darken-2" id="busqueda">Buscar</button>
          </div>
        </div>
      </div> -->

      <div class="Contenedor-admin-tabla">
        <table class="striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Apellido</th>
              <th>Razon Social</th>
              <th>Correo</th>
              <th>Identificación</th>
              <!-- <th>Dirección</th> -->
              <th>Valor</th>
              <th>Descripción</th>
              <th>Referencia</th>
              <th>Metodo</th>
              <th>Estado</th>
              <th>Fecha</th>
            </tr>
          </thead>
          <tbody id="listado-pagos">
          </tbody>
        </table>
      </div>
      <div class="listado-loader">
        <div class="preloader-wrapper big active">
          <div class="spinner-layer">
            <div class="circle-clipper left">
              <div class="circle"></div>
            </div>
            <div class="gap-patch">
              <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
              <div class="circle"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="listado-paginacion">
        <ul class="pagination">
        </ul>
      </div>
    </div>
  </div>

  <script src="../dist/js/jquery.min.js"></script>
  <script src="../dist/js/materialize.js"></script>
  <script src="../dist/js/inicializar.js"></script>
  <script src="../dist/js/jquery-confirm.min.js"></script>
  <script src="../dist/js/jquery.modal.min.js"></script>
  <script src="../dist/js/croppie/croppie.min.js" type="text/javascript"></script>
  <script src="../dist/js/admin-pagos.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
