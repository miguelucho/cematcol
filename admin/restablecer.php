 <!DOCTYPE html>
 <html lang="es">

 <head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta name="keywords" lang="es" content="">
   <meta name="robots" content="All">
   <meta name="description" lang="es" content="">
   <title>Admin | CEMATCOL | Cementos y Materiales de Colombia</title>
   <link rel="stylesheet" href="../dist/css/material-icons.css" />
   <link rel="stylesheet" href="../dist/css/materialize.css" />
   <link rel="stylesheet" href="../dist/css/load.css" />
   <link rel="stylesheet" href="../dist/css/bundled.css" />
   <link rel="stylesheet" href="../dist/css/jquery-confirm.min.css" />
   <link rel="stylesheet" href="../dist/css/administrador.css" />
 </head>

 <body>

   <div class="Conten-login">
     <div class="Conten-login-int">
       <div class="Formulario-login">
         <h2>Restablecer contraseña</h2>
         <img src="../dist/assets/images/logo.png" alt="">
         <div class="Conten-form">
           <form id="recuperar-login">
             <div class="Conten-completo">
               <div class="Conten-uno">
                 <div class="input-field">
                   <input id="email" type="email" name="login[email]" class="validate" required>
                   <label for="email">Correo electrónico</label>
                 </div>
               </div>
             </div>
             <div class="Conten-completo">
               <div class="Conten-uno">
                 <input type="submit" class="btn light-blue darken-2" value="Enviar Mensaje">
               </div>
             </div>
           </form>
           <p>ó</p>
           <a href="login">Iniciar sesión</a>
         </div>
       </div>
     </div>
   </div>
   <script src="../dist/js/jquery.min.js"></script>
   <script src="../dist/js/materialize.js"></script>
   <script src="../dist/js/inicializar.js"></script>
   <script src="../dist/js/jquery-confirm.min.js"></script>
   <script src="../dist/js/restablecer.js?v<?php echo date('YmdHis') ?>"></script>
 </body>

 </html>
