<?php
session_start();
if (!isset($_SESSION['cematcol_user']) or $_SESSION['cematcol_type'] != 'Administrador') {
  header('Location: login');
}
require_once "../dist/libs/conexion.php";

?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords" lang="es" content="">
  <meta name="robots" content="All">
  <meta name="description" lang="es" content="">
  <title>Admin | CEMATCOL | Cementos y Materiales de Colombia</title>
  <link rel="stylesheet" href="../dist/css/material-icons.css" />
  <link rel="stylesheet" href="../dist/css/materialize.css" />
  <link rel="stylesheet" href="../dist/css/load.css" />
  <link rel="stylesheet" href="../dist/css/bundled.css" />
  <link rel="stylesheet" href="../dist/css/jquery-confirm.min.css" />
  <link rel="stylesheet" href="../dist/css/administrador.css" />
</head>

<body>
  <nav>
    <?php include("header-admin.php") ?>
  </nav>
  <div class="Contenedor-admin-global">
    <div class="Contenedor-admin-global-int">
      <div class="Contenedor-boton-crear">
        <div class="Btn-flotante-crear">
          <a href="#" data-target="crearcategoria" data-position="left" data-tooltip="Crear Categoría" class="modal-trigger tooltipped btn-floating btn-large waves-effect waves-light light-blue darken-2"><i class="material-icons">add</i></a>
        </div>
      </div>
      <div class="Contenedor-admin-titulo">
        <h4 class="Titulh4">Categorías</h4>
      </div>

      <div class="Contenedor-admin-texto">
        <p>Crear y administra las categorías, estas serán asignadas luego a los productos.</p>
      </div>

      <div class="Contenedor-admin-texto">
        <div class="Conten-completo">
          <div class="Conten-cuatro">
            <div class="input-field">
              <input id="buscar-nombre" type="text" class="validate">
              <label for="buscar-nombre">Buscar categoría</label>
            </div>
          </div>
          <div class="Conten-cuatro Margin-top">
            <button class="btn light-blue darken-2" id="busqueda">Buscar</button>
          </div>
        </div>
      </div>

      <div class="Contenedor-admin-tabla">
        <table class="striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Orden</th>
              <th>Nombre</th>
              <th>Estado</th>
              <th colspan="2">Acciones</th>
            </tr>
          </thead>

          <tbody id="listado-categorias">
          </tbody>
        </table>
      </div>
      <div class="listado-loader">
        <div class="preloader-wrapper big active">
          <div class="spinner-layer">
            <div class="circle-clipper left">
              <div class="circle"></div>
            </div>
            <div class="gap-patch">
              <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
              <div class="circle"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="listado-paginacion">
        <ul class="pagination">
        </ul>
      </div>
    </div>
  </div>

  <div id="crearcategoria" class="modal">
    <div class="modal-content">
      <h4 class="Titulh4">Crear Categoría</h4>
      <p>Ingresa el nombre de la categoría a crear y selecciona el estado para que pueda ser visualizado en la parte externa de la pagina web.</p>
      <div class="Conten-form-admin">
        <form id="Categoria-nueva">
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <input id="Nombre" type="text" name="categoria[nombre]" class="validate" required>
                <label for="Nombre">Nombre de categoría</label>
              </div>
            </div>
            <div class="Conten-dos">
              <div class="input-field">
                <select name="categoria[estado]">
                  <option value="">Seleccionar</option>
                  <option value="1" selected>Activo</option>
                  <option value="0">Inactivo</option>
                </select>
                <label>Estado de la categoría</label>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <input type="submit" class="btn light-blue darken-2" value="Guardar">
          </div>
        </form>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
    </div>
  </div>

  <div id="editarcategoria" class="modal">
    <div class="modal-content">
      <h4 class="Titulh4">Editar Categoría</h4>
      <p>Ingresa el nombre de la categoría a editar y selecciona el estado para que pueda ser visualizado en la parte externa de la pagina web.</p>
      <div class="Conten-form-admin">
        <form id="Categoria-editar">
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <input id="Nombre-ed" type="text" name="categoria[nombre]" class="validate" required>
                <label for="Nombre-ed">Nombre de categoría</label>
              </div>
            </div>
            <div class="Conten-dos">
              <div class="input-field">
                <select id="Estado-ed" name="categoria[estado]">
                  <option value="">Seleccionar</option>
                  <option value="1" selected>Activo</option>
                  <option value="0">Inactivo</option>
                </select>
                <label>Estado de la categoría</label>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <input type="submit" class="btn light-blue darken-2" value="Guardar">
          </div>
        </form>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
    </div>
  </div>

  <script src="../dist/js/jquery.min.js"></script>
  <script src="../dist/js/materialize.min.js"></script>
  <script src="../dist/js/inicializar.js"></script>
  <script src="../dist/js/jquery-confirm.min.js"></script>
  <script src="../dist/js/categorias.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
