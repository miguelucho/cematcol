<?php
session_start();
if (!isset($_SESSION['cematcol_user'])) {
  header('Location: login');
}
require_once "../dist/libs/conexion.php";

$imagenes = $db
  ->objectBuilder()->get('slider');

$total = $db->count;

?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords" lang="es" content="">
  <meta name="robots" content="All">
  <meta name="description" lang="es" content="">
  <title>Admin | CEMATCOL | Cementos y Materiales de Colombia</title>
  <link rel="stylesheet" href="../dist/css/material-icons.css" />
  <link rel="stylesheet" href="../dist/css/materialize.css" />
  <link rel="stylesheet" href="../dist/css/load.css" />
  <link rel="stylesheet" href="../dist/css/bundled.css" />
  <link rel="stylesheet" href="../dist/css/jquery-confirm.min.css" />
  <link rel="stylesheet" href="../dist/css/jquery.modal.css" />
  <link rel="stylesheet" href="../dist/js/croppie/croppie.css" />
  <link rel="stylesheet" href="../dist/css/administrador.css" />
  <style>
    .modal-size-large {
      width: 100% !important;
    }

    #upload-demo {
      width: 100%;
      height: 80vh;
    }

    .select-dropdown {
      max-height: 300px;
    }
  </style>
</head>

<body>
  <nav>
    <?php include("header-admin.php") ?>
  </nav>
  <div class="Contenedor-admin-global">
    <div class="Contenedor-admin-global-int">
      <div class="Contenedor-boton-crear">
        <input type="hidden" id="imagenes-total" value="<?php echo $total ?>">
        <div class="Btn-flotante-crear">
          <a href="#" data-target="crearslide" data-position="left" data-tooltip="Crear Imagen" class="modal-trigger tooltipped btn-floating btn-large waves-effect waves-light light-blue darken-2"><i class="material-icons">add</i></a>
        </div>
      </div>
      <div class="Contenedor-admin-titulo">
        <h4 class="Titulh4">Slider</h4>
      </div>

      <div class="Contenedor-admin-texto">
        <p>Crear y administra las imágenes que podrás visualizar en el slider de la pagina principal.</p>
      </div>

      <div class="Contenedor-admin-texto">
        <!-- <div class="Conten-completo">
          <div class="Conten-cuatro">
            <div class="input-field">
              <input id="buscar-nombre" type="text" class="validate">
              <label for="buscar-nombre">Buscar marca</label>
            </div>
          </div>
          <div class="Conten-cuatro Margin-top">
            <button class="btn light-blue darken-2" id="busqueda">Buscar</button>
          </div>
        </div> -->
      </div>

      <div class="Contenedor-admin-tabla">
        <table class="striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Orden</th>
              <th>Nombre</th>
              <th>Imagen</th>
              <th>Estado</th>
              <th colspan="2">Acciones</th>
            </tr>
          </thead>
          <tbody id="listado-imagenes">
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div id="crearslide" class="modal">
    <div class="modal-content">
      <h4 class="Titulh4">Crear Slide</h4>
      <p>Carga la imagen que requieras que se visualice en el slider de la pagina principal.</p>
      <div class="Conten-form-admin">
        <form id="Slider-nuevo">
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <input id="Nombre" type="text" name="slider[nombre]" class="validate" required>
                <label for="Nombre">Nombre del slide</label>
              </div>
            </div>
            <div class="Conten-dos">
              <div class="input-field">
                <select name="slider[estado]">
                  <option value="">Seleccionar</option>
                  <option value="1" selected>Activo</option>
                  <option value="0">Inactivo</option>
                </select>
                <label>Estado de la imagen</label>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="file-field input-field">
                <div class="btn">
                  <span>Imagen</span>
                  <input type="file" class="imagen-adjunta">
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate" type="text" placeholder="Cargar imagen" required="">
                </div>
              </div>
            </div>
            <div class="Conten-dos">
            </div>
          </div>
          <div class="Conten-completo">
            <input type="submit" class="btn light-blue darken-2" value="Guardar">
          </div>
        </form>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
    </div>
  </div>

  <div id="editarslide" class="modal">
    <div class="modal-content">
      <h4 class="Titulh4">Editar Slide</h4>
      <p>Carga la imagen que requieras que se visualice en el slider de la pagina principal.</p>
      <div class="Conten-form-admin">
        <form id="Slider-editar">
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <input id="Nombre-ed" type="text" name="slider[nombre]" class="validate" required>
                <label for="Nombre-ed">Nombre del slide</label>
              </div>
            </div>
            <div class="Conten-dos">
              <div class="input-field">
                <select name="slider[estado]" id="Estado-ed">
                  <option value="">Seleccionar</option>
                  <option value="1" selected>Activo</option>
                  <option value="0">Inactivo</option>
                </select>
                <label>Estado de la imagen</label>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="file-field input-field">
                <div class="btn">
                  <span>Imagen</span>
                  <input type="file" class="imagen-adjunta">
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate" type="text" placeholder="Cargar imagen">
                </div>
              </div>
            </div>
            <div class="Conten-dos">
            </div>
          </div>
          <div class="Conten-completo">
            <input type="submit" class="btn light-blue darken-2" value="Guardar">
          </div>
        </form>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
    </div>
  </div>

  <script src="../dist/js/jquery.min.js"></script>
  <script src="../dist/js/materialize.min.js"></script>
  <script src="../dist/js/inicializar.js"></script>
  <script src="../dist/js/jquery-confirm.min.js"></script>
  <script src="../dist/js/jquery.modal.min.js"></script>
  <script src="../dist/js/croppie/croppie.min.js" type="text/javascript"></script>
  <script src="../dist/js/admin-slider.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
