<?php
session_start();
if (!isset($_SESSION['cematcol_user']) or $_SESSION['cematcol_type'] != 'Administrador') {
  header('Location: login');
}
require_once "../dist/libs/conexion.php";

?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords" lang="es" content="">
  <meta name="robots" content="All">
  <meta name="description" lang="es" content="">
  <title>Admin | CEMATCOL | Cementos y Materiales de Colombia</title>
  <link rel="stylesheet" href="../dist/css/material-icons.css" />
  <link rel="stylesheet" href="../dist/css/materialize.css" />
  <link rel="stylesheet" href="../dist/css/load.css" />
  <link rel="stylesheet" href="../dist/css/bundled.css" />
  <link rel="stylesheet" href="../dist/css/jquery-confirm.min.css" />
  <link rel="stylesheet" href="../dist/css/jquery.modal.css" />
  <link rel="stylesheet" href="../dist/js/croppie/croppie.css" />
  <link rel="stylesheet" href="../dist/css/administrador.css" />
  <style>
    #upload-demo {
      width: 100%;
      height: 400px;
    }

    .select-dropdown {
      max-height: 300px;
    }
  </style>
</head>

<body>
  <nav>
    <?php include("header-admin.php") ?>
  </nav>
  <div class="Contenedor-admin-global">
    <div class="Contenedor-admin-global-int">
      <div class="Contenedor-boton-crear">
        <div class="Btn-flotante-crear">
          <a href="#" data-target="crearusuario" data-position="left" data-tooltip="Crear Usuarios" class="modal-trigger tooltipped btn-floating btn-large waves-effect waves-light light-blue darken-2"><i class="material-icons">add</i></a>
        </div>
      </div>
      <div class="Contenedor-admin-titulo">
        <h4 class="Titulh4">Usuarios</h4>
      </div>

      <div class="Contenedor-admin-texto">
        <p>Crear y administra los usuarios.</p>
      </div>

      <div class="Contenedor-admin-texto">
        <div class="Conten-completo">
          <div class="Conten-cuatro">
            <div class="input-field">
              <input id="buscar-nombre" type="text" class="validate">
              <label for="buscar-nombre">Buscar usuario</label>
            </div>
          </div>
          <div class="Conten-cuatro Margin-top">
            <button class="btn light-blue darken-2" id="busqueda">Buscar</button>
          </div>
        </div>
      </div>

      <div class="Contenedor-admin-tabla">
        <table class="striped">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Apellido</th>
              <th>Identificación</th>
              <th>Correo</th>
              <th>Estado</th>
              <th colspan="2">Acciones</th>
            </tr>
          </thead>
          <tbody id="listado-usuarios">
          </tbody>
        </table>
      </div>
      <div class="listado-loader">
        <div class="preloader-wrapper big active">
          <div class="spinner-layer">
            <div class="circle-clipper left">
              <div class="circle"></div>
            </div>
            <div class="gap-patch">
              <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
              <div class="circle"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="listado-paginacion">
        <ul class="pagination">
        </ul>
      </div>
    </div>
  </div>

  <div id="crearusuario" class="modal">
    <div class="modal-content">
      <h4 class="Titulh4">Crear Usuario</h4>
      <div class="Conten-form-admin">
        <form id="Usuario-nuevo">
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <input id="Nombre" type="text" name="usuario[nombre]" class="validate" required>
                <label for="Nombre">Nombre</label>
              </div>
            </div>
            <div class="Conten-dos">
              <div class="input-field">
                <input id="Apellido" type="text" name="usuario[apellido]" class="validate" required>
                <label for="Apellido">Apellido</label>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <input id="Identificacion" type="text" name="usuario[identificacion]" class="validate" required>
                <label for="Identificacion"># Identificación</label>
              </div>
            </div>
            <div class="Conten-dos">
              <div class="input-field">
                <input id="Correo" type="text" name="usuario[correo]" class="validate" required>
                <label for="Correo">Correo</label>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <input id="Password" type="text" name="usuario[password]" class="validate" required>
                <label for="Password">Contraseña</label>
              </div>
            </div>
            <div class="Conten-dos">
              <div class="input-field">
                <select name="usuario[estado]" required>
                  <option value="" disabled selected>Seleccionar</option>
                  <option value="1">Activo</option>
                  <option value="0">Inactivo</option>
                </select>
                <label>Estado</label>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <input type="submit" class="btn light-blue darken-2" value="Guardar">
          </div>
        </form>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
    </div>
  </div>

  <div id="editarusuario" class="modal">
    <div class="modal-content">
      <h4 class="Titulh4">Editar Usuario</h4>
      <div class="Conten-form-admin">
        <form id="Usuario-editar">
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <input id="Nombre-ed" type="text" name="usuario[nombre]" class="validate" required>
                <label for="Nombre-ed">Nombre</label>
              </div>
            </div>
            <div class="Conten-dos">
              <div class="input-field">
                <input id="Apellido-ed" type="text" name="usuario[apellido]" class="validate" required>
                <label for="Apellido-ed">Apellido</label>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <input id="Identificacion-ed" type="text" name="usuario[identificacion]" class="validate" required>
                <label for="Identificacion-ed"># Identificación</label>
              </div>
            </div>
            <div class="Conten-dos">
              <div class="input-field">
                <input id="Correo-ed" type="text" name="usuario[correo]" class="validate" required>
                <label for="Correo-ed">Correo</label>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <input id="Password" type="text" name="usuario[password]" class="validate">
                <label for="Password">Contraseña</label>
              </div>
            </div>
            <div class="Conten-dos">
              <div class="input-field">
                <select id="Estado-ed" name="usuario[estado]" required>
                  <option value="" disabled selected>Seleccionar</option>
                  <option value="1">Activo</option>
                  <option value="0">Inactivo</option>
                </select>
                <label>Estado</label>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <input type="submit" class="btn light-blue darken-2" value="Guardar">
          </div>
        </form>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
    </div>
  </div>

  <script src="../dist/js/jquery.min.js"></script>
  <script src="../dist/js/materialize.js"></script>
  <script src="../dist/js/inicializar.js"></script>
  <script src="../dist/js/jquery-confirm.min.js"></script>
  <script src="../dist/js/jquery.modal.min.js"></script>
  <script src="../dist/js/croppie/croppie.min.js" type="text/javascript"></script>
  <script src="../dist/js/usuarios.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
