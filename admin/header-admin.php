<div class="nav-wrapper">
  <?php
  $usuarios = $db
    ->where('Id_us', $_SESSION['cematcol_user'])
    ->objectBuilder()->get('usuarios');
  ?>
  <a href="#" class="brand-logo Logo-admin"><?php echo $usuarios[0]->nombre_us . ' ' . $usuarios[0]->apellido_us; ?></a>
  <ul id="nav-mobile" class="right hide-on-med-and-down">
    <li><a href="admin-pagos">Pagos</a></li>
    <?php
    if ($_SESSION['cematcol_type'] == 'Administrador') {
    ?>
      <li><a href="admin-marcas">Marcas</a></li>
      <li><a href="admin-categorias">Categorías</a></li>
      <li><a href="admin-productos">Productos</a></li>
      <li><a href="admin-slider">Slider</a></li>
      <li><a href="admin-usuarios">Usuarios</a></li>
    <?php
    }
    ?>
    <li><a href="../dist/libs/logout">Cerrar sesión</a></li>
  </ul>
</div>
