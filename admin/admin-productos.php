<?php
session_start();
if (!isset($_SESSION['cematcol_user']) or $_SESSION['cematcol_type'] != 'Administrador') {
  header('Location: login');
}
require_once "../dist/libs/conexion.php";
$ls_marcas = '';
$ls_categorias = '';

$marcas = $db
  ->orderBy('nombre_m', 'ASC')
  ->objectBuilder()->get('marcas');

foreach ($marcas as $marca) {
  $ls_marcas .= '<option value="' . $marca->Id_m . '">' . $marca->nombre_m . '</option>';
}

$categorias = $db
  ->orderBy('nombre_c', 'ASC')
  ->objectBuilder()->get('categorias');

foreach ($categorias as $categoria) {
  $ls_categorias .= '<option value="' . $categoria->Id_c . '">' . $categoria->nombre_c . '</option>';
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords" lang="es" content="">
  <meta name="robots" content="All">
  <meta name="description" lang="es" content="">
  <title>Admin | CEMATCOL | Cementos y Materiales de Colombia</title>
  <link rel="stylesheet" href="../dist/css/material-icons.css" />
  <link rel="stylesheet" href="../dist/css/materialize.css" />
  <link rel="stylesheet" href="../dist/css/load.css" />
  <link rel="stylesheet" href="../dist/css/bundled.css" />
  <link rel="stylesheet" href="../dist/css/jquery-confirm.min.css" />
  <link rel="stylesheet" href="../dist/css/jquery.modal.css" />
  <link rel="stylesheet" href="../dist/js/croppie/croppie.css" />
  <link rel="stylesheet" href="../dist/css/administrador.css" />
  <style>
    #upload-demo {
      width: 100%;
      height: 400px;
    }

    .select-dropdown {
      max-height: 300px;
    }
  </style>
</head>

<body>
  <nav>
    <?php include("header-admin.php") ?>
  </nav>
  <div class="Contenedor-admin-global">
    <div class="Contenedor-admin-global-int">
      <div class="Contenedor-boton-crear">
        <div class="Btn-flotante-crear">
          <a href="#" data-target="crearproducto" data-position="left" data-tooltip="Crear Productos" class="modal-trigger tooltipped btn-floating btn-large waves-effect waves-light light-blue darken-2"><i class="material-icons">add</i></a>
        </div>
      </div>
      <div class="Contenedor-admin-titulo">
        <h4 class="Titulh4">Productos</h4>
      </div>

      <div class="Contenedor-admin-texto">
        <p>Crear y administra los productos, verifica que el estado sea activo para que se puedan visualizar en el catalogo.</p>
      </div>

      <div class="Contenedor-admin-texto">
        <div class="Conten-completo">
          <div class="Conten-cuatro">
            <div class="input-field">
              <input id="buscar-nombre" type="text" class="validate">
              <label for="buscar-nombre">Buscar producto</label>
            </div>
          </div>
          <div class="Conten-cuatro Margin-top">
            <button class="btn light-blue darken-2" id="busqueda">Buscar</button>
          </div>
        </div>
      </div>

      <div class="Contenedor-admin-tabla">
        <table class="striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Categoría</th>
              <th>Marca</th>
              <th>Nombre</th>
              <th>Imagen</th>
              <th>Estado</th>
              <th colspan="2">Acciones</th>
            </tr>
          </thead>
          <tbody id="listado-productos">
          </tbody>
        </table>
      </div>
      <div class="listado-loader">
        <div class="preloader-wrapper big active">
          <div class="spinner-layer">
            <div class="circle-clipper left">
              <div class="circle"></div>
            </div>
            <div class="gap-patch">
              <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
              <div class="circle"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="listado-paginacion">
        <ul class="pagination">
        </ul>
      </div>
    </div>
  </div>

  <div id="crearproducto" class="modal">
    <div class="modal-content">
      <h4 class="Titulh4">Crear Producto</h4>
      <p>Ingresa el nombre del producto a crear y selecciona el estado para que pueda ser visualizado en la parte externa de la pagina web.</p>
      <div class="Conten-form-admin">
        <form id="Producto-nuevo">
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <select name="producto[categoria]">
                  <option value="" disabled selected>Seleccionar</option>
                  <?php echo $ls_categorias ?>
                </select>
                <label>Categoría</label>
              </div>
            </div>
            <div class="Conten-dos">
              <div class="input-field">
                <select name="producto[marca]">
                  <option value="" disabled selected>Seleccionar</option>
                  <?php echo $ls_marcas ?>
                </select>
                <label>Marca</label>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <input id="Nombre" type="text" name="producto[nombre]" class="validate">
                <label for="Nombre">Nombre de producto</label>
              </div>
            </div>
            <div class="Conten-dos">
              <div class="file-field input-field">
                <div class="btn">
                  <span>Imagen</span>
                  <input type="file" class="imagen-adjunta">
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate" type="text" placeholder="Cargar imagen" required>
                </div>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <select name="producto[estado]">
                  <option value="" disabled selected>Seleccionar</option>
                  <option value="1">Activo</option>
                  <option value="0">Inactivo</option>
                </select>
                <label>Estado del producto</label>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <div class="Conten-uno">
              <div class="input-field">
                <textarea id="Descripcion" class="materialize-textarea" name="producto[describe]"></textarea>
                <label for="Descripcion">Ingresar descripción del producto</label>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <input type="submit" class="btn light-blue darken-2" value="Guardar">
          </div>
        </form>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
    </div>
  </div>

  <div id="editarproducto" class="modal">
    <div class="modal-content">
      <h4 class="Titulh4">Editar Producto</h4>
      <p>Ingresa el nombre del producto a editar y selecciona el estado para que pueda ser visualizado en la parte externa de la pagina web.</p>
      <div class="Conten-form-admin">
        <form id="Producto-editar">
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <select name="producto[categoria]" id="Categoria-ed">
                  <option value="" disabled selected>Seleccionar</option>
                  <?php echo $ls_categorias ?>
                </select>
                <label>Categoría</label>
              </div>
            </div>
            <div class="Conten-dos">
              <div class="input-field">
                <select name="producto[marca]" id="Marca-ed">
                  <option value="" disabled selected>Seleccionar</option>
                  <?php echo $ls_marcas ?>
                </select>
                <label>Marca</label>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <input id="Nombre-ed" type="text" name="producto[nombre]" class="validate">
                <label for="Nombre-ed">Nombre de producto</label>
              </div>
            </div>
            <div class="Conten-dos">
              <div class="file-field input-field">
                <div class="btn">
                  <span>Imagen</span>
                  <input type="file" class="imagen-adjunta">
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate" type="text" placeholder="Cargar imagen">
                </div>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <div class="Conten-dos">
              <div class="input-field">
                <select name="producto[estado]" id="Estado-ed">
                  <option value="" disabled selected>Seleccionar</option>
                  <option value="1">Activo</option>
                  <option value="0">Inactivo</option>
                </select>
                <label>Estado del producto</label>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <div class="Conten-uno">
              <div class="input-field">
                <textarea id="Descripcion-ed" class="materialize-textarea" name="producto[describe]"></textarea>
                <label for="Descripcion-ed">Ingresar descripción del producto</label>
              </div>
            </div>
          </div>
          <div class="Conten-completo">
            <input type="submit" class="btn light-blue darken-2" value="Guardar">
          </div>
        </form>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cerrar</a>
    </div>
  </div>

  <script src="../dist/js/jquery.min.js"></script>
  <script src="../dist/js/materialize.js"></script>
  <script src="../dist/js/inicializar.js"></script>
  <script src="../dist/js/jquery-confirm.min.js"></script>
  <script src="../dist/js/jquery.modal.min.js"></script>
  <script src="../dist/js/croppie/croppie.min.js" type="text/javascript"></script>
  <script src="../dist/js/productos.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
