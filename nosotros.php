<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords" lang="es" content="">
  <meta name="robots" content="All">
  <meta name="description" lang="es" content="">
  <title>Nosotros | CEMATCOL | Cementos y Materiales de Colombia</title>
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" href="dist/css/load.css" />
  <link rel="stylesheet" href="dist/css/bundled.css" />
  <link rel="stylesheet" href="dist/css/easy-autocomplete.css">
  <link rel="stylesheet" href="dist/css/jquery-confirm.min.css" />
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-QHGYVVW2HJ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-QHGYVVW2HJ');
  </script>
</head>

<body>
  <header>
    <?php include("dist/libs/pages/header-top.php") ?>
  </header>

  <section>
    <div class="Mashead">
      <div class="Mashead-int">
        <div class="Mashead-int-sombra">
          <div class="Mashead-int-texto">
            <div class="Mashead-int-texto-con">
              <h1>NOSOTROS</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="Seccion-nosotros">
      <div class="Seccion-nosotros-int-doble">
        <div class="Seccion-nosotros-int-doble-sec">
          <p>Con más de 20 años de experiencia en el sector de distribución de materiales para la construcción, somos la empresa líder en el sur de Bogotá que provee los insumos de obra negra de mayor calidad a Ferreterías y Obras. </p>
          <p>
            Contamos con las mejores marcas de la industria, un equipo altamente capacitado y rápidos tiempos de entrega
          </p>
          <p>Nuestro compromiso! Aportar al desarrollo y progreso de la capital Colombiana y sus municipios aledaños.</p>
        </div>
        <div class="Seccion-nosotros-int-doble-sec">
          <img src="dist/assets/images/acerca_cematcol.jpg" alt="">
        </div>
      </div>

      <!-- <div class="Seccion-nosotros-int-sencillo">
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
      </div> -->
    </div>
  </section>

  <section>
    <?php include("dist/libs/pages/marcas-aliadas.php") ?>
  </section>

  <section>
    <?php include("dist/libs/pages/contacto.php") ?>
  </section>
  <section>
    <?php include("dist/libs/pages/whatsapp.php") ?>
  </section>

  <script src="dist/js/jquery.min.js"></script>
  <script src="dist/js/menu-public.js"></script>
  <script src="dist/js/TweenMax.min.js"></script>
  <script src="dist/js/aos.js"></script>
  <script src="dist/js/jquery.superslides.min.js"></script>
  <script src="dist/js/jquery-confirm.min.js"></script>
  <script src="dist/js/mapa-contacto-google.js"></script>
  <script src="dist/js/jquery.easy-autocomplete.min.js"></script>
  <script src="dist/js/smooth-scroll.polyfills.js"></script>
  <script>
    var scroll = new SmoothScroll('a[href*="#"]', {

      // Selectors
      ignore: '[data-scroll-ignore]', // Selector for links to ignore (must be a valid CSS selector)
      header: null, // Selector for fixed headers (must be a valid CSS selector)
      topOnEmptyHash: true, // Scroll to the top of the page for links with href="#"

      // Speed & Duration
      speed: 500, // Integer. Amount of time in milliseconds it should take to scroll 1000px
      speedAsDuration: false, // If true, use speed as the total duration of the scroll animation
      durationMax: null, // Integer. The maximum amount of time the scroll animation should take
      durationMin: null, // Integer. The minimum amount of time the scroll animation should take
      clip: true, // If true, adjust scroll distance to prevent abrupt stops near the bottom of the page
      offset: 160,


      easing: 'easeInOutCubic', // Easing pattern to use
      customEasing: function(time) {


        return time < 0.5 ? 2 * time * time : -1 + (4 - 2 * time) * time;

      },

      // History
      updateURL: true, // Update the URL on scroll
      popstate: true, // Animate scrolling with the forward/backward browser buttons (requires updateURL to be true)

      // Custom Events
      emitEvents: true // Emit custom events

    });

    $(document).ready(function() {
      $('#Fil-marca').on('click', function() {
        console.log('Click');
        $('.Seccion-prodmarcas-int-contenedor-sec1').toggleClass('Ocultar-filtro')

      });
    });

    AOS.init();
    $(function() {
      $('#slides').superslides({
        inherit_width_from: '.wide-container',
        inherit_height_from: '.wide-container',
        play: 3000,
        animation: 'fade'
      });
    });

    var options = {
      url: function(phrase) {
        return "dist/libs/ac_listados?producto[action]=Productos-busqueda&producto[bsq]=" + phrase + "&format=json";
      },
      getValue: "nombre",
      list: {
        match: {
          enabled: true
        },
        onChooseEvent: function() {
          $('<form action="productos" method="GET"><input name="bsq" value="' + 'Pr-' + $("#Buscador").getSelectedItemData().ID + '-Nm-' + $("#Buscador").getSelectedItemData().nombre + '"></form>').appendTo('body').submit();
        }
      }
    };

    $('#Buscador').easyAutocomplete(options);
  </script>


  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfdouGws62iXdkOXVek4RmrdTQSuOJs38&callback=initMap" type="text/javascript"></script>
</body>

</html>