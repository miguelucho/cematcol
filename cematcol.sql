/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : cematcol

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 20/05/2021 17:49:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for categorias
-- ----------------------------
DROP TABLE IF EXISTS `categorias`;
CREATE TABLE `categorias`  (
  `Id_c` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_c` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `estado_c` tinyint(1) NULL DEFAULT NULL,
  `posicion_c` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id_c`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categorias
-- ----------------------------
INSERT INTO `categorias` VALUES (1, 'CEMENTOS', 1, '1');
INSERT INTO `categorias` VALUES (2, 'HIERROS', 1, '2');
INSERT INTO `categorias` VALUES (3, 'ESTRUCTURAL', 1, '3');
INSERT INTO `categorias` VALUES (4, 'BLOQUE', 1, '4');
INSERT INTO `categorias` VALUES (5, 'TEJA ZINC', 1, '5');
INSERT INTO `categorias` VALUES (6, 'TEJAS FIBROCEMENTO', 1, '6');
INSERT INTO `categorias` VALUES (7, 'TEJAS PLÁSTICAS', 1, '7');
INSERT INTO `categorias` VALUES (8, 'STEEL DECK', 1, '8');
INSERT INTO `categorias` VALUES (9, 'MALLA', 1, '9');
INSERT INTO `categorias` VALUES (10, 'TUBERÍA RECTANGULAR Y CUADRADA', 1, '10');
INSERT INTO `categorias` VALUES (11, 'PUNTILLAS', 1, '11');
INSERT INTO `categorias` VALUES (12, 'ESTUCOS', 1, '12');
INSERT INTO `categorias` VALUES (13, 'TUBO SISTEMA PVC', 1, '13');
INSERT INTO `categorias` VALUES (14, 'ÁNGULOS', 1, '14');
INSERT INTO `categorias` VALUES (15, 'PLACA FÁCIL', 1, '15');

-- ----------------------------
-- Table structure for marcas
-- ----------------------------
DROP TABLE IF EXISTS `marcas`;
CREATE TABLE `marcas`  (
  `Id_m` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_m` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `estado_m` tinyint(1) NULL DEFAULT NULL,
  `posicion_m` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id_m`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of marcas
-- ----------------------------
INSERT INTO `marcas` VALUES (1, 'CEMEX', 1, '1');
INSERT INTO `marcas` VALUES (2, 'DIACO', 1, '2');
INSERT INTO `marcas` VALUES (3, 'SANTA FE', 1, '3');
INSERT INTO `marcas` VALUES (4, 'COLMENA', 1, '4');
INSERT INTO `marcas` VALUES (5, 'SUPERMASTICK', 1, '5');
INSERT INTO `marcas` VALUES (6, 'TERNIUM', 1, '6');
INSERT INTO `marcas` VALUES (7, 'ALMASA', 1, '7');
INSERT INTO `marcas` VALUES (8, 'AJOVER', 1, '8');
INSERT INTO `marcas` VALUES (9, 'GERFOR', 1, '9');
INSERT INTO `marcas` VALUES (10, 'CORPACERO', 1, '10');
INSERT INTO `marcas` VALUES (11, 'LA CAMPANA', 1, '11');
INSERT INTO `marcas` VALUES (12, 'PROTEJA', 1, '12');
INSERT INTO `marcas` VALUES (13, 'MALLASFER', 1, '13');
INSERT INTO `marcas` VALUES (14, 'PERFILAM', 1, '14');
INSERT INTO `marcas` VALUES (15, 'PERFIACEROS', 1, '15');
INSERT INTO `marcas` VALUES (16, 'PRODALCO', 1, '16');

-- ----------------------------
-- Table structure for productos
-- ----------------------------
DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos`  (
  `Id_p` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_p` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `categoria_p` int(11) NULL DEFAULT NULL,
  `marca_p` int(11) NULL DEFAULT NULL,
  `imagen_p` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `estado_p` tinyint(1) NULL DEFAULT NULL,
  `descripcion_p` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `vistas_p` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Id_p`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of productos
-- ----------------------------
INSERT INTO `productos` VALUES (1, 'producto 1', 14, 7, 'productos/20210520214220big_0be290753779633ce21d50881a901671d81c2d19.jpg', 1, 'adfadf', NULL);
INSERT INTO `productos` VALUES (2, 'producto 2', 1, 4, 'productos/2021052021423436497-1920x1080.jpg', 1, 'adffd f ', NULL);
INSERT INTO `productos` VALUES (3, 'producto 3', 1, 4, 'productos/20210520214246big_446b2b34524acfb2ca857e4c3eea4370b888c78f.jpg', 1, 'adffdf', NULL);
INSERT INTO `productos` VALUES (4, 'producto 4', 3, 11, 'productos/2021052021425936361-1920x1080.jpg', 1, 'ffff', 1);
INSERT INTO `productos` VALUES (5, 'producto 5', 3, 16, 'productos/2021052021433034052-2000x1414.jpg', 1, 'fdafdf', 2);

-- ----------------------------
-- Table structure for slider
-- ----------------------------
DROP TABLE IF EXISTS `slider`;
CREATE TABLE `slider`  (
  `Id_s` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_s` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `imagen_s` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `estado_s` tinyint(1) NULL DEFAULT NULL,
  `posicion_s` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Id_s`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of slider
-- ----------------------------
INSERT INTO `slider` VALUES (1, 'imagen 1', 'slider/2021052021085652fcccea3e27aa87.jpg', 1, '1');
INSERT INTO `slider` VALUES (2, 'imagen 2', 'slider/202105202109421920x1080-px-cityscape-digital-art-futuristic-city-spaceship-1345447-wallherecom.jpg', 1, '2');
INSERT INTO `slider` VALUES (4, 'imagen 3', 'slider/2021052021220557360-2560x1600.jpg', 1, '3');
INSERT INTO `slider` VALUES (7, 'imagen 4', 'slider/20210520213500big_446b2b34524acfb2ca857e4c3eea4370b888c78f.jpg', 1, '4');
INSERT INTO `slider` VALUES (10, 'imagen 5', 'slider/20210520213651build_2-wallpaper-3840x2400.jpg', 1, '5');

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios`  (
  `Id_us` int(11) NOT NULL AUTO_INCREMENT,
  `login_us` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password_us` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ultimo_acceso_us` datetime NOT NULL,
  PRIMARY KEY (`Id_us`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES (1, 'admin@gmail.com', '$2y$10$wJ.pXDr7HYwCWFLuFvRyQuFNO7/HwL07sVlrEJVVTnC1ODpi/bcPi', '2021-05-20 16:45:03');

SET FOREIGN_KEY_CHECKS = 1;
