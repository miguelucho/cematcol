<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords" lang="es" content="">
  <meta name="robots" content="All">
  <meta name="description" lang="es" content="">
  <title>Pagos en lÍnea | CEMATCOL | Cementos y Materiales de Colombia</title>
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" href="dist/css/load.css" />
  <link rel="stylesheet" href="dist/css/bundled.css" />
  <link rel="stylesheet" href="dist/css/easy-autocomplete.css">
  <link rel="stylesheet" href="dist/css/jquery-confirm.min.css" />
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-QHGYVVW2HJ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-QHGYVVW2HJ');
  </script>
</head>

<body>
  <header>
    <?php include("dist/libs/pages/header-top.php") ?>
  </header>

  <section>
    <div class="Mashead">
      <div class="Mashead-int">
        <div class="Mashead-int-sombra">
          <div class="Mashead-int-texto">
            <div class="Mashead-int-texto-con">
              <h1>PAGOS EN LÍNEA</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="Pagoslinea">
      <div class="Pagoslinea__int">
        <p>Ingresa la información a continuación para realizar un pago en línea</p>
        <p>Los campo con asterisco <span class="Obliga">*</span> son obligatorios.</p>
        <form id="Form-pagos">
          <div class="Pagoslinea__form">
            <div class="Forms Flex">
              <div class="Forms__sec">
                <label for="Pago-nombres">Nombres <span class="Obliga">*</span></label>
                <input type="text" placeholder="Nombres" name="pago[nombre]" id="Pago-nombres" required>
                <label for="Pago-apellidos">Apellidos <span class="Obliga">*</span></label>
                <input type="text" placeholder="Apellidos" name="pago[apellidos]" id="Pago-apellidos" required>
                <label for="Pago-ident">Nº Identificación CC ó NIT<span class="Obliga">*</span></label>
                <input type="text" placeholder="Identificación" name="pago[identificacion]" id="Pago-ident" required>
              </div>
              <div class="Forms__sec">
                <label for="Pago-razonsocial">Razón social <span class="Obliga"></span></label>
                <input type="text" placeholder="Razón social" name="pago[razonsocial]" id="Pago-razonsocial">
                <label for="Pago-correo">Correo electrónico <span class="Obliga">*</span></label>
                <input type="email" placeholder="Correo electrónico" name="pago[correo]" required id="Pago-correo">
                <!-- <label for="Pago-direccion">Dirección <span class="Obliga">*</span></label>
                <input type="text" placeholder="Dirección" name="pago[direccion]" required id="Pago-direccion"> -->
              </div>
            </div>
            <div class="Forms">
              <label for="Pago-valor">Valor a pagar <span class="Obliga">*</span></label>
              <input type="number" placeholder="Valor" name="pago[valor]" required id="Pago-valor">
            </div>
            <div class="Forms">
              <label for="Pago-descripcion">Descripción <span class="Obliga">*</span></label>
              <textarea name="pago[descripcion]" id="Pago-descripcion" placeholder="Ejemplo: Número de factura" required></textarea>
            </div>
            <div class="Forms">
              <p><input type="checkbox" id="myCheck" name="pago[terminos]" required> <a href="https://www.cematcol.com/politicas" target="_blank" class="Fpoliticas">Acepta nuestras políticas y tratamiento de datos</a></p>
            </div>
            <div class="Forms">
              <button type="submit" class="Btn-rojo-redondo">Realizar pago</button>
            </div>
          </div>
        </form>

      </div>
    </div>
  </section>

  <section>
    <?php include("dist/libs/pages/marcas-aliadas.php") ?>
  </section>

  <section>
    <?php include("dist/libs/pages/contacto.php") ?>
  </section>
  <section>
    <?php include("dist/libs/pages/whatsapp.php") ?>
  </section>

  <script src="dist/js/jquery.min.js"></script>
  <script src="dist/js/menu-public.js"></script>
  <script src="dist/js/TweenMax.min.js"></script>
  <script src="dist/js/aos.js"></script>
  <script src="dist/js/jquery.superslides.min.js"></script>
  <script src="dist/js/jquery-confirm.min.js"></script>
  <script src="dist/js/mapa-contacto-google.js"></script>
  <script src="dist/js/jquery.easy-autocomplete.min.js"></script>
  <script src="dist/js/smooth-scroll.polyfills.js"></script>
  <script>
    var scroll = new SmoothScroll('a[href*="#"]', {

      // Selectors
      ignore: '[data-scroll-ignore]', // Selector for links to ignore (must be a valid CSS selector)
      header: null, // Selector for fixed headers (must be a valid CSS selector)
      topOnEmptyHash: true, // Scroll to the top of the page for links with href="#"

      // Speed & Duration
      speed: 500, // Integer. Amount of time in milliseconds it should take to scroll 1000px
      speedAsDuration: false, // If true, use speed as the total duration of the scroll animation
      durationMax: null, // Integer. The maximum amount of time the scroll animation should take
      durationMin: null, // Integer. The minimum amount of time the scroll animation should take
      clip: true, // If true, adjust scroll distance to prevent abrupt stops near the bottom of the page
      offset: 160,


      easing: 'easeInOutCubic', // Easing pattern to use
      customEasing: function(time) {


        return time < 0.5 ? 2 * time * time : -1 + (4 - 2 * time) * time;

      },

      // History
      updateURL: true, // Update the URL on scroll
      popstate: true, // Animate scrolling with the forward/backward browser buttons (requires updateURL to be true)

      // Custom Events
      emitEvents: true // Emit custom events

    });

    $(document).ready(function() {
      $('#Fil-marca').on('click', function() {
        console.log('Click');
        $('.Seccion-prodmarcas-int-contenedor-sec1').toggleClass('Ocultar-filtro')

      });
    });

    AOS.init();
    $(function() {
      $('#slides').superslides({
        inherit_width_from: '.wide-container',
        inherit_height_from: '.wide-container',
        play: 3000,
        animation: 'fade'
      });
    });

    var options = {
      url: function(phrase) {
        return "dist/libs/ac_listados?producto[action]=Productos-busqueda&producto[bsq]=" + phrase + "&format=json";
      },
      getValue: "nombre",
      list: {
        match: {
          enabled: true
        },
        onChooseEvent: function() {
          $('<form action="productos" method="GET"><input name="bsq" value="' + 'Pr-' + $("#Buscador").getSelectedItemData().ID + '-Nm-' + $("#Buscador").getSelectedItemData().nombre + '"></form>').appendTo('body').submit();
        }
      }
    };

    $('#Buscador').easyAutocomplete(options);
  </script>


  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfdouGws62iXdkOXVek4RmrdTQSuOJs38&callback=initMap" type="text/javascript"></script>
  <script type="text/javascript" src="https://checkout.wompi.co/widget.js"></script>
  <script src="dist/js/pagos.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
