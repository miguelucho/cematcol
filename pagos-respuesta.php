<?php
require_once "dist/libs/conexion.php";

$datos = file_get_contents('php://input');

$keys = $db
    ->objectBuilder()->get('wompi_keys');

$decodedData = json_decode($datos, true);
$referencia = $decodedData['data']['transaction']['reference'];
$estado = $decodedData['data']['transaction']['status'];
$metodo = $decodedData['data']['transaction']['payment_method_type'];


$transactionId = $decodedData['data']['transaction']['id'];
$transactionAmount = $decodedData['data']['transaction']['amount_in_cents'];

$cadena_concatenada = $transactionId.$estado.$transactionAmount.$decodedData['timestamp'].$keys[0]->eventos;
$signature = hash ("sha256", $cadena_concatenada);

if($signature == $decodedData['signature']['checksum']) {
    $db
        ->where('referencia_pg', $referencia)
        ->update('pagos', ['estado_pg' => $estado, 'metodo_pg' => $metodo, 'transaccionid_pg' => $transactionId]);
}

header('Content-Type: application/json');
http_response_code(200);
echo json_encode(['status' => 'success']);

