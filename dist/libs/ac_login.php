<?php
require_once "conexion.php";

$data   = $_REQUEST['login'];
$msg    = [];

switch ($data['action']) {
    case 'user-login':
        $login = $data['email'];

        $check = $db
            ->where('login_us', $login)
            ->where('estado_us', '1')
            ->objectBuilder()->get('usuarios');
        if ($db->count > 0) {
            if (password_verify($data['password'], $check[0]->password_us)) {
                session_start();
                $_SESSION['cematcol_user'] = $check[0]->Id_us;
                $_SESSION['cematcol_type'] = $check[0]->tipo_us;

                $db
                    ->where('Id_us', $check[0]->Id_us)
                    ->update('usuarios', ['ultimo_acceso_us' => $db->now()]);

                $msg['status']   = true;

                if ($_SESSION['cematcol_type'] == 'Asesor') {
                    $msg['redirect'] = 'admin-pagos';
                } else {
                    $msg['redirect'] = 'admin-productos';
                }
            } else {
                $msg['status'] = false;
                $msg['msg']    = 'Error, Email o contraseña incorrectos';
            }
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, el usuario no existe';
        }
        echo json_encode($msg);
        break;
    case 'reset-pass':
        $login = $data['email'];

        $check = $db
            ->where('login_us', $login)
            ->objectBuilder()->get('usuarios');

        if ($db->count > 0) {
            include "Password.php";
            $npass     = RandomPassword();
            $tmppass   = password_hash($npass, PASSWORD_BCRYPT);
            $actualiza = $db
                ->where('Id_us', $check[0]->Id_us)
                ->update('usuarios', ['password_us' => $tmppass]);

            $cabeceras = 'From: noreply@cematcol.com';
            $asunto    = "Restablecer contraseña Cematcol Ltda";
            $email_to  = $check[0]->login_us;
            $contenido = "Restablecer contraseña\n"
                . "\n"
                . "Hemos recibido una petición para restablecer la contraseña de tu cuenta.\n"
                . "Tu nueva contraseña es: $npass\n";

            mail($email_to, $asunto, $contenido, $cabeceras);

            $msg['status']   = true;
            $msg['msg'] = 'Se te ha enviado un email con tu nueva contraseña.';
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, el usuario no existe';
        }
        echo json_encode($msg);
        break;
}

function RandomPassword($length = 6)
{
    $chars    = "abcdefghijkmnpqrstuvwxyz23456789";
    $charsLen = strlen($chars);
    $pass     = '';
    for ($i = 0; $i < $length; $i++) {
        $pass .= substr($chars, mt_rand(0, $charsLen - 1), 1);
    }
    return $pass;
}
