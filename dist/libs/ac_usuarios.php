<?php
require_once "conexion.php";

$data   = $_REQUEST['usuario'];
$msg    = [];

switch ($data['action']) {
    case 'Usuario-nuevo':
        $check = $db
            ->where('correo_us', $data['correo'])
            ->objectBuilder()->get('usuarios');

        if ($db->count == 0) {
            $password = password_hash($data['password'], PASSWORD_BCRYPT);

            $datos = [
                'login_us' => $data['correo'],
                'password_us' => $password,
                'nombre_us' => $data['nombre'],
                'apellido_us' => $data['apellido'],
                'identificacion_us' => $data['identificacion'],
                'correo_us' => $data['correo'],
                'tipo_us' => 'Asesor',
                'estado_us' => $data['estado']
            ];

            $nuevo = $db
                ->insert('usuarios', $datos);

            if ($nuevo) {
                $msg['status']   = true;
                $msg['msg'] = 'Usuario creado';
            } else {
                $msg['status'] = false;
                $msg['msg']    = 'Error, no se pudo crear el usuario';
            }
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, el usuario ya existe!';
        }

        echo json_encode($msg);
        break;
    case 'Usuario-lista':
        $page       = $data['pagina'];
        $results_pg = 30;
        $adjacent   = 2;

        $nombre    = $db->escape(trim($data['nombre']));

        ($nombre == '' ? $nombre = '%' : '');

        $totalitems = $db
            ->where('COALESCE(nombre_us, "")', '%' . $nombre . '%', 'LIKE')
            ->where('tipo_us', 'Asesor')
            ->objectBuilder()->get('usuarios');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            require_once 'Paginacion.php';

            $content       = '';
            $db->pageLimit = $results_pg;

            $listing = $db
                ->where('COALESCE(nombre_us, "")', '%' . $nombre . '%', 'LIKE')
                ->where('tipo_us', 'Asesor')
                ->orderBy('nombre_us', 'ASC')
                ->objectBuilder()->paginate('usuarios', $page);

            foreach ($listing as $usuario) {
                $estado = '';

                switch ($usuario->estado_us) {
                    case '0':
                        $estado = 'Inactivo';
                        break;
                    case '1':
                        $estado = 'Activo';
                        break;
                }

                $content .= '<tr id="Us-' . $usuario->Id_us . '">
                                <td>' . $usuario->nombre_us . '</td>
                                <td>' . $usuario->apellido_us . '</td>
                                <td>' . $usuario->identificacion_us . '</td>
                                <td>' . $usuario->correo_us . '</td>
                                <td>' . $estado . '</td>
                                <td> <a href="#!" class="waves-effect waves-light btn light-blue darken-2 Editar-usuario">Editar</a></td>
                                <td> <a href="#!" class="waves-effect waves-light btn grey lighten-1 Eliminar-usuario">Eliminar</a></td>
                            </tr>';
            }

            $msg['list']       = $content;
            $pagconfig         = array('pagina' => $page, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpgs, 'resultados_pag' => $results_pg, 'adyacentes' => $adjacent);
            $paginate          = new Paginacion($pagconfig);
            $msg['pagination'] = $paginate->crearlinks();
        } else {
            $msg['list'] = '<tr>
                                <td colspan="6">No hay registros</td>
                            </tr>';
            $msg['pagination'] = '';
        }

        echo json_encode($msg);
        break;
    case 'Usuario-info':
        $idusuario = explode('-', $data['idusuario']);

        $check = $db
            ->where('Id_us', $idusuario[1])
            ->objectBuilder()->get('usuarios');

        if ($db->count > 0) {
            $usuarios = $db
                ->where('Id_us', $idusuario[1])
                ->objectBuilder()->get('usuarios');

            $msg['status'] = true;
            $msg['info']   = $usuarios;
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, el usuario no existe!';
        }

        echo json_encode($msg);
        break;
    case 'Usuario-editar':
        $idusuario = explode('-', $data['idusuario']);

        $check = $db
            ->where('correo_us', $data['correo'])
            ->where('Id_us', $idusuario[1], '!=')
            ->objectBuilder()->get('usuarios');

        if ($db->count == 0) {
            $datos = [
                'login_us' => $data['identificacion'],
                'nombre_us' => $data['nombre'],
                'apellido_us' => $data['apellido'],
                'identificacion_us' => $data['identificacion'],
                'correo_us' => $data['correo'],
                'estado_us' => $data['estado']
            ];

            if ($data['password'] != '') {
                $password = password_hash($data['password'], PASSWORD_BCRYPT);
                $datos['password_us'] = $password;
            }

            $editar = $db
                ->where('Id_us', $idusuario[1])
                ->update('usuarios', $datos);

            if ($editar) {
                $msg['status']   = true;
                $msg['msg'] = 'Usuario editado';
            } else {
                $msg['status'] = false;
                $msg['msg']    = 'Error, no se pudo editar el producto';
            }
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, el usuario ya existe!';
        }

        echo json_encode($msg);
        break;
    case 'Usuario-eliminar':
        $idusuario = explode('-', $data['idusuario']);

        $usuarios = $db
            ->where('Id_us', $idusuario[1])
            ->delete('usuarios');

        if ($usuarios) {
            $msg['status'] = true;
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, el usuario no existe!';
        }

        echo json_encode($msg);
        break;
}


function limpiar($String)
{
    $String = preg_replace("/[^A-Za-z0-9\_\-]/", '', $String);
    return $String;
}
