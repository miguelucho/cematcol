<?php
require_once "conexion.php";

$data   = $_REQUEST['contacto'];
$msg    = [];

$cabeceras = 'From: noreply@cematcol.com';
$asunto    = "Mensaje de Contacto";
$email_to  = 'cematcolltda@gmail.com';
$contenido = "Datos de Contacto\n"
    . "\n"
    . "Nombre: $data[nombre]\n"
    . "Teléfono: $data[telefono]\n"
    . "Correo electrónico: $data[email]\n"
    . "Mensaje: $data[mensaje]\n";

if (mail($email_to, $asunto, $contenido, $cabeceras)) {
    $msg['status']   = true;
    $msg['msg'] = 'Tu mensaje ha sido enviado!';
} else {
    $msg['status'] = false;
    $msg['msg']    = 'Error, tu mensaje no pudo set enviado';
}

echo json_encode($msg);
