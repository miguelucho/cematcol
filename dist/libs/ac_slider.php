<?php
require_once "conexion.php";

$data   = $_REQUEST['slider'];
$msg    = [];
$folder = '../slider/';

switch ($data['action']) {
    case 'Slider-nuevo':
        $datos = [
            'nombre_s' => $data['nombre'],
            'estado_s' => $data['estado'],
        ];

        if (isset($data['imagen'])) {
            $img        = explode(',', $data['imagen']);
            $img        = base64_decode($img[1]);
            $nombre     = date('YmdHis') . Limpiar($data['imagen_nombre']) . '.jpg';
            $archivador = $folder . $nombre;

            if (file_put_contents($archivador, $img)) {
                require 'imagine/vendor/autoload.php';
                $imagine = new Imagine\Gd\Imagine();

                $img = $imagine->open($archivador);
                $img = $img->save($archivador, ['jpeg_quality' => 100]);

                $datos['imagen_s'] = substr($archivador, 3);
            }
        }

        $nuevo = $db
            ->insert('slider', $datos);

        if ($nuevo) {
            $imagenes = $db
                ->objectBuilder()->get('slider');

            $total = $db->count;
            $msg['total'] = $total;

            $msg['status']   = true;
            $msg['msg'] = 'Imagen creada';
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, no se pudo crear la imagen';
        }

        echo json_encode($msg);
        break;
    case 'Slider-lista':
        $page       = $data['pagina'];
        $results_pg = 30;
        $adjacent   = 2;

        $totalitems = $db
            ->objectBuilder()->get('slider');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            require_once 'Paginacion.php';

            $content       = '';
            $db->pageLimit = $results_pg;

            $listing = $db
                ->orderBy('Id_s', 'DESC')
                ->objectBuilder()->paginate('slider', $page);

            foreach ($listing as $item) {
                $estado = '';

                switch ($item->estado_s) {
                    case '0':
                        $estado = 'Inactivo';
                        break;
                    case '1':
                        $estado = 'Activo';
                        break;
                }

                $content .= '<tr id="S-' . $item->Id_s . '">
                                <td>' . $item->Id_s . '</td>
                                <td><input type="number" step="1" min="1" class="orden" style="width:60px" value="' . $item->posicion_s . '"></td>
                                <td>' . $item->nombre_s . '</td>
                                <td><a href="../dist/' . $item->imagen_s . '" target="_blank" class="waves-effect waves-light btn light-blue teal darken-1">Ver</a></td>
                                <td>' . $estado . '</td>
                                <td> <a href="#!" class="waves-effect waves-light btn light-blue darken-2 Editar-imagen">Editar</a></td>
                                <td> <a href="#!" class="waves-effect waves-light btn grey lighten-1 Eliminar-imagen">Eliminar</a></td>
                            </tr>';
            }

            $msg['list']       = $content;
            $pagconfig         = array('pagina' => $page, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpgs, 'resultados_pag' => $results_pg, 'adyacentes' => $adjacent);
            $paginate          = new Paginacion($pagconfig);
            $msg['pagination'] = $paginate->crearlinks();
        } else {
            $msg['list'] = '<tr>
                                <td colspan="6">No hay registros</td>
                            </tr>';
            $msg['pagination'] = '';
        }

        echo json_encode($msg);
        break;
    case 'Imagen-info':
        $idimagen = explode('-', $data['idimagen']);

        $check = $db
            ->where('Id_s', $idimagen[1])
            ->objectBuilder()->get('slider');

        if ($db->count > 0) {
            $productos = $db
                ->where('Id_s', $idimagen[1])
                ->objectBuilder()->get('slider');

            $msg['status'] = true;
            $msg['info']   = $productos;
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, la imagen no existe!';
        }

        echo json_encode($msg);
        break;
    case 'Slider-editar':
        $idimagen = explode('-', $data['idimagen']);

        $datos = [
            'nombre_s' => $data['nombre'],
            'estado_s' => $data['estado'],
        ];

        if (isset($data['imagen'])) {
            $img        = explode(',', $data['imagen']);
            $img        = base64_decode($img[1]);
            $nombre     = date('YmdHis') . Limpiar($data['imagen_nombre']) . '.jpg';
            $archivador = $folder . $nombre;

            if (file_put_contents($archivador, $img)) {
                require 'imagine/vendor/autoload.php';
                $imagine = new Imagine\Gd\Imagine();

                $img = $imagine->open($archivador);
                $img = $img->save($archivador, ['jpeg_quality' => 100]);

                $imagenes = $db
                    ->where('Id_s', $idimagen[1])
                    ->objectBuilder()->get('slider');

                if ($imagenes[0]->imagen_s != '') {
                    if (file_exists('../' . $imagenes[0]->imagen_s)) {
                        unlink('../' . $imagenes[0]->imagen_s);
                    }
                }

                $datos['imagen_s'] = substr($archivador, 3);
            }
        }

        $editar = $db
            ->where('Id_s', $idimagen[1])
            ->update('slider', $datos);

        if ($editar) {
            $msg['status']   = true;
            $msg['msg'] = 'Imagen editada';
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, no se pudo editar la imagen';
        }

        echo json_encode($msg);
        break;
    case 'Slider-eliminar':
        $idimagen = explode('-', $data['idimagen']);

        $check = $db
            ->where('Id_s', $idimagen[1])
            ->objectBuilder()->get('slider');

        if ($db->count > 0) {
            if ($check[0]->imagen_s != '') {
                if (file_exists('../' . $check[0]->imagen_s)) {
                    unlink('../' . $check[0]->imagen_s);
                }
            }

            $imagenes = $db
                ->where('Id_s', $idimagen[1])
                ->delete('slider');

            $msg['status'] = true;

            $imagenes = $db
                ->objectBuilder()->get('slider');

            $total = $db->count;
            $msg['total'] = $total;
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, la imagen no existe!';
        }

        echo json_encode($msg);
        break;
    case 'Slider-posicion':
        $idimagen = explode('-', $data['idimagen']);

        $check = $db
            ->where('Id_s', $idimagen[1])
            ->objectBuilder()->get('slider');

        if ($db->count > 0) {
            $slider = $db
                ->where('Id_s', $idimagen[1])
                ->update('slider', ['posicion_s' => $data['posicion']]);

            $msg['status'] = true;
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, la imagen no existe!';
        }

        echo json_encode($msg);
        break;
}


function limpiar($String)
{
    $String = preg_replace("/[^A-Za-z0-9\_\-]/", '', $String);
    return $String;
}
