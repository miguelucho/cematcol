<?php
require_once "conexion.php";

$data   = $_REQUEST['pago'];
$msg    = [];

switch ($data['opc']) {
    case 'Registro-pago':
        $referencia = createRandomCode();

        $existe_ref = 0;

        do {
            $ventas = $db
                ->where('referencia_pg', $referencia)
                ->objectBuilder()->get('pagos');

            if ($db->count > 0) {
                $referencia = createRandomCode();
            } else {
                $existe_ref = 1;
            }
        } while ($existe_ref <= 0);

        $datos = [
            'nombre_pg' => $data['nombre'],
            'apellidos_pg' => $data['apellidos'],
            'razonsocial_pg' => $data['razonsocial'],
            'correo_pg' => $data['correo'],
            'identificacion_pg' => $data['identificacion'],
            // 'direccion_pg' => $data['direccion'],
            'valor_pg' => $data['valor'],
            'descripcion_pg' => $data['descripcion'],
            'referencia_pg' => $referencia,
            'estado_pg' => '',
            'fechapago_pg' => date('Y-m-d H:i:s'),

        ];

        $nuevo = $db
            ->insert('pagos', $datos);

        if ($nuevo) {
            $msg['status']   = true;
            $msg['msg'] = 'Registro creado';

            $keys = $db
                ->objectBuilder()->get('wompi_keys');


            $cadena_concatenada = $referencia . $data['valor'] . '00' . 'COP' . $keys[0]->integridad;
            $signature = hash("sha256", $cadena_concatenada);

            $infopago = [
                'monto' => $data['valor'] . '00',
                'referencia' => $referencia,
                'publicKey' => $keys[0]->publica,
                'signature' => $signature,
                'correo' => $data['correo'],
                'nombre' => $data['nombre'] . ' ' . $data['apellidos'],
                'identificacion' => $data['identificacion']
            ];

            $msg['infopago'] = $infopago;
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, no se pudo crear el registro';
        }

        echo json_encode($msg);
        break;
    case 'Pagos-lista':
        $page       = $data['pagina'];
        $results_pg = 50;
        $adjacent   = 2;

        $nombre    = $db->escape(trim($data['nombre']));

        ($nombre == '' ? $nombre = '%' : '');

        $totalitems = $db
            ->where('nombre_pg', '%' . $nombre . '%', 'LIKE')
            ->objectBuilder()->get('pagos');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            require_once 'Paginacion.php';

            $content       = '';
            $db->pageLimit = $results_pg;

            $listing = $db
                ->where('nombre_pg', '%' . $nombre . '%', 'LIKE')
                ->orderBy('Id_pg', 'DESC')
                ->objectBuilder()->paginate('pagos', $page);

            foreach ($listing as $pagos) {
                $estado = '';

                switch ($pagos->estado_pg) {
                    case 'APPROVED':
                        $estado = 'Aprobado';
                        break;
                    case 'VOIDED':
                        $estado = 'Rechazado';
                        break;
                    case 'DECLINED ':
                        $estado = 'Declinado';
                        break;
                    case 'ERROR':
                        $estado = 'Error';
                        break;
                }

                $content .= '<tr>
                                <td>' . $pagos->Id_pg . '</td>
                                <td>' . $pagos->nombre_pg . '</td>
                                <td>' . $pagos->apellidos_pg . '</td>
                                <td>' . $pagos->razonsocial_pg . '</td>
                                <td>' . $pagos->correo_pg . '</td>
                                <td>' . $pagos->identificacion_pg . '</td>                                
                                <td>' . $pagos->valor_pg . '</td>
                                <td>' . $pagos->descripcion_pg . '</td>
                                <td>' . $pagos->referencia_pg . '</td>
                                <td>' . $pagos->metodo_pg . '</td>
                                <td>' . $estado . '</td>
                                <td>' . $pagos->fechapago_pg . '</td>
                            </tr>';
            }

            $msg['list']       = $content;
            $pagconfig         = array('pagina' => $page, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpgs, 'resultados_pag' => $results_pg, 'adyacentes' => $adjacent);
            $paginate          = new Paginacion($pagconfig);
            $msg['pagination'] = $paginate->crearlinks();
        } else {
            $msg['list'] = '<tr>
                                <td colspan="13">No hay registros</td>
                            </tr>';
            $msg['pagination'] = '';
        }

        echo json_encode($msg);
        break;
}


function createRandomCode()
{

    $chars = "ABCDEFGHIJKMNOPQRSTUVWXYZ023456789";
    srand((float) microtime() * 1000000);
    $i    = 0;
    $pass = '';

    while ($i <= 10) {
        $num  = rand() % 33;
        $tmp  = substr($chars, $num, 1);
        $pass = $pass . $tmp;
        $i++;
    }

    return $pass;
}
