<?php
require_once "conexion.php";

$data   = $_REQUEST['marca'];
$msg    = [];

switch ($data['action']) {
    case 'Marca-nuevo':
        $check = $db
            ->where('nombre_m', $data['nombre'])
            ->objectBuilder()->get('marcas');

        if ($db->count == 0) {
            $datos = [
                'nombre_m' => $data['nombre'],
                'estado_m' => $data['estado'],
            ];

            $nuevo = $db
                ->insert('marcas', $datos);

            if ($nuevo) {
                $msg['status']   = true;
                $msg['msg'] = 'Marca creada';
            } else {
                $msg['status'] = false;
                $msg['msg']    = 'Error, no se pudo crear la marca';
            }
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, la marca ya existe!';
        }

        echo json_encode($msg);
        break;
    case 'Marca-lista':
        $page       = $data['pagina'];
        $results_pg = 30;
        $adjacent   = 2;

        $nombre    = $db->escape(trim($data['nombre']));

        ($nombre == '' ? $nombre = '%' : '');

        $totalitems = $db
            ->where('nombre_m', '%' . $nombre . '%', 'LIKE')
            ->objectBuilder()->get('marcas');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            require_once 'Paginacion.php';

            $content       = '';
            $db->pageLimit = $results_pg;

            $listing = $db
                ->where('nombre_m', '%' . $nombre . '%', 'LIKE')
                ->orderBy('Id_m', 'DESC')
                ->objectBuilder()->paginate('marcas', $page);

            foreach ($listing as $marca) {
                $estado = '';

                switch ($marca->estado_m) {
                    case '0':
                        $estado = 'Inactivo';
                        break;
                    case '1':
                        $estado = 'Activo';
                        break;
                }

                $content .= '<tr id="M-' . $marca->Id_m . '">
                                <td>' . $marca->Id_m . '</td>
                                <td><input type="number" step="1" min="1" class="orden" style="width:60px" value="' . $marca->posicion_m . '"></td>
                                <td>' . $marca->nombre_m . '</td>
                                <td>' . $estado . '</td>
                                <td> <a href="#!" class="waves-effect waves-light btn light-blue darken-2 Editar-marca">Editar</a></td>
                                <td> <a href="#!" class="waves-effect waves-light btn grey lighten-1 Eliminar-marca">Eliminar</a></td>
                            </tr>';
            }

            $msg['list']       = $content;
            $pagconfig         = array('pagina' => $page, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpgs, 'resultados_pag' => $results_pg, 'adyacentes' => $adjacent);
            $paginate          = new Paginacion($pagconfig);
            $msg['pagination'] = $paginate->crearlinks();
        } else {
            $msg['list'] = '<tr>
                                <td colspan="6">No hay registros</td>
                            </tr>';
            $msg['pagination'] = '';
        }

        echo json_encode($msg);
        break;
    case 'Marca-info':
        $idmarca = explode('-', $data['idmarca']);

        $check = $db
            ->where('Id_m', $idmarca[1])
            ->objectBuilder()->get('marcas');

        if ($db->count > 0) {
            $marcas = $db
                ->where('Id_m', $idmarca[1])
                ->objectBuilder()->get('marcas');

            $msg['status'] = true;
            $msg['info']   = $marcas;
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, la marca no existe!';
        }

        echo json_encode($msg);
        break;
    case 'Marca-editar':
        $idmarca = explode('-', $data['idmarca']);

        $check = $db
            ->where('nombre_m', $data['nombre'])
            ->where('Id_m', $idmarca[1], '!=')
            ->objectBuilder()->get('marcas');

        if ($db->count == 0) {
            $datos = [
                'nombre_m' => $data['nombre'],
                'estado_m' => $data['estado'],
            ];

            $editar = $db
                ->where('Id_m', $idmarca[1])
                ->update('marcas', $datos);

            if ($editar) {
                $msg['status']   = true;
                $msg['msg'] = 'Marca editada';
            } else {
                $msg['status'] = false;
                $msg['msg']    = 'Error, no se pudo editar la marca';
            }
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, la marca ya existe!';
        }

        echo json_encode($msg);
        break;
    case 'Marca-eliminar':
        $idmarca = explode('-', $data['idmarca']);

        $check = $db
            ->where('Id_m', $idmarca[1])
            ->objectBuilder()->get('marcas');

        if ($db->count > 0) {
            $marcas = $db
                ->where('Id_m', $idmarca[1])
                ->delete('marcas');

            $msg['status'] = true;
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, la marca no existe!';
        }

        echo json_encode($msg);
        break;
    case 'Marca-posicion':
        $idmarca = explode('-', $data['idmarca']);

        $check = $db
            ->where('Id_m', $idmarca[1])
            ->objectBuilder()->get('marcas');

        if ($db->count > 0) {
            $marcas = $db
                ->where('Id_m', $idmarca[1])
                ->update('marcas', ['posicion_m' => $data['posicion']]);

            $msg['status'] = true;
            $msg['info']   = $marcas;
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, la marca no existe!';
        }

        echo json_encode($msg);
        break;
}
