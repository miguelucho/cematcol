<?php
require_once "conexion.php";

$data   = $_REQUEST['producto'];
$msg    = [];

switch ($data['action']) {
    case 'Productos-lista':
        $page       = $data['pagina'];
        $results_pg = 24;
        $adjacent   = 2;

        if ($data['categoria'] != '0') {
            $categoria = explode('-', $data['categoria']);
            $categoria = $categoria[1];
        } else {
            $categoria = '%';
        }

        if ($data['producto'] != '0') {
            $idproducto = explode('-', $data['producto']);
            $idproducto = $idproducto[1];
        } else {
            $idproducto = '%';
        }

        $totalitems = $db
            ->where('estado_p', 1)
            ->where('Id_p', $idproducto, 'LIKE')
            ->where('categoria_p', $categoria, 'LIKE')
            ->objectBuilder()->get('productos');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            require_once 'Paginacion.php';

            $content       = '';
            $db->pageLimit = $results_pg;
            $cont = 0;

            $listing = $db
                ->where('estado_p', 1)
                ->where('Id_p', $idproducto, 'LIKE')
                ->where('categoria_p', $categoria, 'LIKE')
                ->orderBy('Id_p', 'DESC')
                ->objectBuilder()->paginate('productos', $page);

            foreach ($listing as $producto) {
                $marca = '';

                $marcas = $db
                    ->where('Id_m', $producto->marca_p)
                    ->objectBuilder()->get('marcas');

                if ($db->count > 0) {
                    $marca = $marcas[0]->nombre_m;
                }

                if ($cont == 0) {
                    $content .= '<div class="Productos-destacados-contenedor  Sin-margin-top">';
                }

                if ($cont < 4) {
                    $content .= '<div class="Producto-ficha Producto-min" data-aos="fade-up" data-aos-delay="580" data-aos-duration="1500" data-producto="Pr-' . $producto->Id_p . '">
                                <div class="Producto-ficha-arriba">
                                <div class="Producto-ficha-imagen">
                                    <img src="dist/' . $producto->imagen_p . '" alt="">
                                </div>
                                </div>
                                <div class="Producto-ficha-abajo">
                                <div class="Producto-ficha-descripcion">
                                    <div class="Producto-ficha-nombre">
                                    <span>' . $producto->nombre_p . '</span>
                                    </div>
                                    <div class="Producto-ficha-marca">
                                    <span>' . $marca . '</span>
                                    </div>
                                    <div class="Producto-ficha-contenbtn">
                                    <a href="#!" class="Btn-rojo-redondo Btn-block Ver-mas" data-producto="Pr-' . $producto->Id_p . '"> Saber Mas</a>
                                    </div>
                                </div>
                                </div>
                            </div>';

                    $cont++;
                }
                if ($cont == 4) {
                    $content .= '</div>';
                    $cont = 0;
                }

                // if ($cont % 4) {
                //     $content .= '</div>';
                // }
            }

            $msg['list']       = $content;
            $pagconfig         = array('pagina' => $page, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpgs, 'resultados_pag' => $results_pg, 'adyacentes' => $adjacent);
            $paginate          = new Paginacion($pagconfig);
            $msg['pagination'] = $paginate->crearlinks();
        } else {
            $msg['list'] = '<p style="text-align:center">No se encontraron productos.</p>';
            $msg['pagination'] = '';
        }

        echo json_encode($msg);
        break;
    case 'Marcas-lista':
        $page       = $data['pagina'];
        $results_pg = 100;
        $adjacent   = 2;

        if ($data['marca'] != '0') {
            $marca = explode('-', $data['marca']);
            $marca = $marca[1];
        } else {
            $marca = '%';
        }

        if ($data['producto'] != '0') {
            $idproducto = explode('-', $data['producto']);
            $idproducto = $idproducto[1];
        } else {
            $idproducto = '%';
        }

        $totalitems = $db
            ->where('estado_p', 1)
            ->where('marca_p', $marca, 'LIKE')
            ->where('Id_p', $idproducto, 'LIKE')
            ->objectBuilder()->get('productos');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            require_once 'Paginacion.php';

            $content       = '';
            $db->pageLimit = $results_pg;
            $cont = '';

            $listing = $db
                ->where('estado_p', 1)
                ->where('marca_p', $marca, 'LIKE')
                ->where('Id_p', $idproducto, 'LIKE')
                ->orderBy('Id_p', 'DESC')
                ->objectBuilder()->paginate('productos', $page);

            foreach ($listing as $producto) {
                $marca = '';

                $marcas = $db
                    ->where('Id_m', $producto->marca_p)
                    ->objectBuilder()->get('marcas');

                if ($db->count > 0) {
                    $marca = $marcas[0]->nombre_m;
                }

                if ($cont == 0) {
                    $content .= '<div class="Productos-destacados-contenedor  Sin-margin-top">';
                }

                if ($cont < 4) {
                    $content .= '<div class="Producto-ficha Producto-min" data-aos="fade-up" data-aos-delay="580" data-aos-duration="1500" data-producto="Pr-' . $producto->Id_p . '">
                                <div class="Producto-ficha-arriba">
                                    <div class="Producto-ficha-imagen">
                                        <img src="dist/' . $producto->imagen_p . '" alt="">
                                    </div>
                                </div>
                                <div class="Producto-ficha-abajo">
                                    <div class="Producto-ficha-descripcion">
                                        <div class="Producto-ficha-nombre">
                                        <span>' . $producto->nombre_p . '</span>
                                        </div>
                                        <div class="Producto-ficha-marca">
                                        <span>' . $marca . '</span>
                                        </div>
                                        <div class="Producto-ficha-contenbtn">
                                        <a href="#!" class="Btn-rojo-redondo Btn-block Ver-mas" data-producto="Pr-' . $producto->Id_p . '"> Saber Mas</a>
                                        </div>
                                    </div>
                                </div>
                            </div>';

                    $cont++;
                }
                if ($cont == 4) {
                    $content .= '</div>';
                    $cont = 0;
                }

                // if ($total % 4) {
                //     $content .= '</div>';
                // }
            }

            $msg['list']       = $content;
            $pagconfig         = array('pagina' => $page, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpgs, 'resultados_pag' => $results_pg, 'adyacentes' => $adjacent);
            $paginate          = new Paginacion($pagconfig);
            $msg['pagination'] = $paginate->crearlinks();
        } else {
            $msg['list'] = '<p style="text-align:center">No se encontraron productos.</p>';
            $msg['pagination'] = '';
        }

        echo json_encode($msg);
        break;
    case 'Producto-info':
        $idproducto = explode('-', $data['producto']);

        $check = $db
            ->where('Id_p', $idproducto[1])
            ->objectBuilder()->get('productos');

        if ($db->count > 0) {
            $total = $check[0]->vistas_p + 1;

            $productos = $db
                ->where('Id_p', $idproducto[1])
                ->update('productos', ['vistas_p' => $total]);

            // $msg['q'] = $db->getLastQuery();
            $marca = '';

            $marcas = $db
                ->where('Id_m', $check[0]->marca_p)
                ->objectBuilder()->get('marcas');

            if ($db->count > 0) {
                $marca = $marcas[0]->nombre_m;
            }

            $productos = $db
                ->where('Id_p', $idproducto[1])
                ->objectBuilder()->get('productos', null, 'descripcion_p AS descripcion, imagen_p AS imagen, nombre_p AS nombre');

            $productos[0]->marca = $marca;

            $msg['status'] = true;
            $msg['info']   = $productos;
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, el producto no existe!';
        }

        echo json_encode($msg);
        break;
    case 'Productos-busqueda':
        $productos = $db
            ->where('estado_p', 1)
            ->where('nombre_p', '%' . $data['bsq'] . '%', 'LIKE')
            ->objectBuilder()->get('productos', null, 'Id_p AS ID, nombre_p AS nombre');

        // $msg['info'] = $productos;

        echo json_encode($productos);
        break;
}
