<div class="Pagosonline">
  <div class="Pagosonline__int">
    <div class="Marcas-titulo">
      <h3>Pagos en linea</h3>
    </div>
    <p>Realiza pagos en linea de forma segura mediante PSE o tarjetas de crédito.</p>
    <div class="Pagosonline__boton">
      <a href="pagosenlinea" target="_blank" class="Btn-rojo-redondo">Paga en linea</a>
    </div>
  </div>
</div>
<div class="Contacto" id="contactenos">
  <div class="Contacto-int">
    <div class="Contacto-titulo">
      <h3>Contacto</h3>
    </div>
    <div class="Contacto-secciones">
      <div class="Contacto-secciones-int">
        <div class="Form-cematcol">
          <form id="Form-contacto" class="Forms-contacto">
            <label for="">Nombre</label>
            <input type="text" placeholder="Nombre" name="contacto[nombre]" id="" required>
            <label for="">Teléfono</label>
            <input type="text" placeholder="Teléfono" name="contacto[telefono]" id="" required>
            <label for="">Correo electrónico</label>
            <input type="email" placeholder="Email" name="contacto[email]" id="">
            <label for="">Mensaje</label>
            <textarea placeholder="Mensaje" name="contacto[mensaje]" required></textarea>
            <input type="checkbox" id="cbox2" value="second_checkbox" class="Checkline" required> <label for="cbox2" class="Checkline">Esta de acuerdo con nuestras <a href="politicas" class="Vin-poli" target="_blank">políticas, condiciones y tratamiento de datos</a>.</label>
            <input type="submit" class="Btn-rojo-redondo M-top" value="Enviar mensaje">
          </form>
        </div>
      </div>
      <div class="Contacto-secciones-int">
        <div class="Mapa">
          <div class="Mapa-int">
            <div id="map">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<footer>
  <div class="Pie">
    <p>Copyright © CEMATCOL, 2021. All rights reserved Designed by <a href="https://www.inngeniate.com/" target="_blank">Inngeniate.com</a> </p>
  </div>
</footer>