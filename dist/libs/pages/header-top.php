<div class="Top">
  <div class="Top-superior">
    <div class="Top-superior-int">
      <div class="Top-superior-int-sec1"></div>
      <div class="Top-superior-int-sec2">
        <div class="Top-superior-int-sec2-contacto">
          <ul>
            <li> <a href="tel:+0317140867"> <i class="icon-phone"></i> PBX 7140867</a> <a href="tel:+573114840613">311 484 06 13</a> <a href="tel:+573232321186">323 232 11 86</a> </li>
            <li> <a href="mailto:cematcolltda@gmail.com"><i class="icon-envelop"></i> cematcolltda@gmail.com</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="Top-inferior">
    <div class="Top-inferior-int">
      <div class="Top-inferior-int-sec">
        <a href="https://www.cematcol.com/test"> <img src="dist/assets/images/logo.png" class="Logo-cem" alt="logo Cematcol"> </a>
      </div>
      <div class="Top-inferior-int-sec Top-sec-dos">
        <div class="Top-inferior-conten">
          <div class="Top-inferior-conten-buscador input-field">
            <input type="text" class="Top-buscador autocomplete" id="Buscador" placeholder="Buscar">
            <i class="icon-search Top-lupita"></i>
          </div>
          <div class="Top-inferior-conten-buscador">
            <a href="pagosenlinea" target="_blank" class="Btn-rojo-redondo">Paga en linea</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<nav>
  <div class="Menu-horizontal">
    <div class="Menu-horizontal-int">
      <div class="Menu-drop Menu-oculto">
        <a href="javascript:void(0)" id="Drop">
          <img src="dist/assets/images/menu_hamburgesa.png" alt="">
        </a>
      </div>
      <div class="Menu-completo Menu-completo-oculto">
        <ul>
          <li> <a href="https://www.cematcol.com/test/"><i class="icon-home"></i> Inicio</a> </li>
          <li> <a href="productos"><i class="icon-price-tag"></i> Productos</a> </li>
          <li> <a href="marcas"><i class="icon-briefcase"></i> Marcas</a> </li>
          <li> <a href="nosotros"><i class="icon-user"></i> Nosotros</a> </li>
          <li> <a href="#contactenos"><i class="icon-envelop"></i> Contacto</a> </li>
          <li class="Ocultabtn"> <a href="pagosenlinea" target="_blank" class="Btn-rojo-redondo">Paga en linea</a></li>
        </ul>
      </div>
    </div>
  </div>
</nav>