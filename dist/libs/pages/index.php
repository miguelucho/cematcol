<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords" lang="es" content="">
  <meta name="robots" content="All">
  <meta name="description" lang="es" content="">
  <title>CEMATCOL | Cementos y Materiales de Colombia</title>
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
</head>

<body>
  <header>
    <?php include("dist/libs/pages/header-top.php") ?>
  </header>

  <div class="Banner">
    <?php include("dist/libs/pages/banner-home.php") ?>
  </div>


  <section>
    <div class="Productos-destacados">
      <div class="Productos-destacados-int">
        <div class="Productos-destacados-titulo">
          <h3>Productos Destacados</h3>
        </div>
        <div class="Productos-destacados-contenedor">
          <div class="Producto-ficha" data-aos="fade-up" data-aos-delay="580" data-aos-duration="1500">
            <div class="Producto-ficha-arriba">
              <div class="Producto-ficha-imagen">
                <img src="dist/assets/images/productos/producto_1.jpg" alt="">
              </div>
            </div>
            <div class="Producto-ficha-abajo">
              <div class="Producto-ficha-descripcion">
                <div class="Producto-ficha-nombre">
                  <span>Cemento Gris 50Kg pero si el nombre es largo</span>
                </div>
                <div class="Producto-ficha-marca">
                  <span>Cemex</span>
                </div>
                <div class="Producto-ficha-contenbtn">
                  <a href="#!" class="Btn-rojo-redondo Btn-block"> Saber Mas</a>
                </div>
              </div>
            </div>
          </div>

          <div class="Producto-ficha" data-aos="fade-up" data-aos-delay="580" data-aos-duration="1500">
            <div class="Producto-ficha-arriba">
              <div class="Producto-ficha-imagen">
                <img src="dist/assets/images/productos/producto_1.jpg" alt="">
              </div>
            </div>
            <div class="Producto-ficha-abajo">
              <div class="Producto-ficha-descripcion">
                <div class="Producto-ficha-nombre">
                  <span>Cemento Gris 50Kg pero si el nombre es largo</span>
                </div>
                <div class="Producto-ficha-marca">
                  <span>Cemex</span>
                </div>
                <div class="Producto-ficha-contenbtn">
                  <a href="#!" class="Btn-rojo-redondo Btn-block"> Saber Mas</a>
                </div>
              </div>
            </div>
          </div>

          <div class="Producto-ficha" data-aos="fade-up" data-aos-delay="580" data-aos-duration="1500">
            <div class="Producto-ficha-arriba">
              <div class="Producto-ficha-imagen">
                <img src="dist/assets/images/productos/producto_1.jpg" alt="">
              </div>
            </div>
            <div class="Producto-ficha-abajo">
              <div class="Producto-ficha-descripcion">
                <div class="Producto-ficha-nombre">
                  <span>Cemento Gris 50Kg pero si el nombre es largo</span>
                </div>
                <div class="Producto-ficha-marca">
                  <span>Cemex</span>
                </div>
                <div class="Producto-ficha-contenbtn">
                  <a href="#!" class="Btn-rojo-redondo Btn-block"> Saber Mas</a>
                </div>
              </div>
            </div>
          </div>

          <div class="Producto-ficha" data-aos="fade-up" data-aos-delay="580" data-aos-duration="1500">
            <div class="Producto-ficha-arriba">
              <div class="Producto-ficha-imagen">
                <img src="dist/assets/images/productos/producto_1.jpg" alt="">
              </div>
            </div>
            <div class="Producto-ficha-abajo">
              <div class="Producto-ficha-descripcion">
                <div class="Producto-ficha-nombre">
                  <span>Cemento Gris 50Kg pero si el nombre es largo</span>
                </div>
                <div class="Producto-ficha-marca">
                  <span>Cemex</span>
                </div>
                <div class="Producto-ficha-contenbtn">
                  <a href="#!" class="Btn-rojo-redondo Btn-block"> Saber Mas</a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="Productos-conten-vermas">
          <a href="#!" class="Btn-negro-redondo">Ver todos</a>
        </div>


      </div>
    </div>
  </section>

  <section id="Estadist">
    <?php include("dist/libs/pages/estadisticas.php") ?>
  </section>

  <section>
    <?php include("dist/libs/pages/marcas-aliadas.php") ?>
  </section>

  <section>
    <?php include("dist/libs/pages/contacto.php") ?>
  </section>

  <script src="dist/js/jquery.min.js"></script>
  <script src="dist/js/menu-public.js"></script>
  <script src="dist/js/TweenMax.min.js"></script>
  <script src="dist/js/aos.js"></script>
  <script src="dist/js/jquery.superslides.min.js"></script>
  <script src="dist/js/mapa-contacto-google.js"></script>
  <script>
    AOS.init();
    $(function() {
      $('#slides').superslides({
        inherit_width_from: '.wide-container',
        inherit_height_from: '.wide-container',
        play: 3000,
        animation: 'fade'
      });
    });
  </script>
  <script>
    let video = document.querySelector('#Estadist');

    let observer = new IntersectionObserver((entries, observer) => {
      entries.forEach(entry => {
        var counter = {
          var: 0
        };
        var nombre = TweenMax.to(counter, 1, {
          var: 5,
          onUpdate: function() {
            var number = Math.ceil(counter.var);
            $('.counter').html(number);
            if (number === counter.var) {
              nombre.kill();
            }
          },
          onComplete: function() {},
          ease: Circ.easeOut
        });

        var counter2 = {
          var: 0
        };
        var nombre = TweenMax.to(counter2, 20, {
          var: 600,
          onUpdate: function() {
            var number = Math.ceil(counter2.var);
            $('.counter2').html(number);
            if (number === counter2.var) {
              nombre.kill();
            }
          },
          onComplete: function() {},
          ease: Circ.easeOut
        });

        var counter3 = {
          var: 0
        };
        var nombre = TweenMax.to(counter3, 1, {
          var: 1,
          onUpdate: function() {
            var number = Math.ceil(counter3.var);
            $('.counter3').html(number);
            if (number === counter3.var) {
              nombre.kill();
            }
          },
          onComplete: function() {},
          ease: Circ.easeOut
        });

        var counter4 = {
          var: 0
        };
        var nombre = TweenMax.to(counter4, 3, {
          var: 150,
          onUpdate: function() {
            var number = Math.ceil(counter4.var);
            $('.counter4').html(number);
            if (number === counter4.var) {
              nombre.kill();
            }
          },
          onComplete: function() {},
          ease: Circ.easeOut
        });

        var counter5 = {
          var: 0
        };
        var nombre = TweenMax.to(counter5, 1, {
          var: 300,
          onUpdate: function() {
            var number = Math.ceil(counter5.var);
            $('.counter5').html(number);
            if (number === counter5.var) {
              nombre.kill();
            }
          },
          onComplete: function() {},
          ease: Circ.easeOut
        });


      });
    }, {
      threshold: 1
    });
    observer.observe(video);
  </script>


  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfdouGws62iXdkOXVek4RmrdTQSuOJs38&callback=initMap" type="text/javascript"></script>
</body>

</html>