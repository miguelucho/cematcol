<?php
require_once "conexion.php";

$data   = $_REQUEST['producto'];
$msg    = [];
$folder = '../productos/';

switch ($data['action']) {
    case 'Producto-nuevo':
        $check = $db
            ->where('nombre_p', $data['nombre'])
            ->objectBuilder()->get('productos');

        if ($db->count == 0) {
            $datos = [
                'nombre_p' => $data['nombre'],
                'marca_p' => $data['marca'],
                'estado_p' => $data['estado'],
                'descripcion_p' => $data['describe'],
                'categoria_p' => $data['categoria'],
            ];

            if (isset($data['imagen'])) {
                $img        = explode(',', $data['imagen']);
                $img        = base64_decode($img[1]);
                $nombre     = date('YmdHis') . Limpiar($data['imagen_nombre']) . '.jpg';
                $archivador = $folder . $nombre;

                if (file_put_contents($archivador, $img)) {
                    require 'imagine/vendor/autoload.php';
                    $imagine = new Imagine\Gd\Imagine();

                    $img = $imagine->open($archivador);
                    $img = $img->save($archivador);

                    $datos['imagen_p'] = substr($archivador, 3);
                }
            }

            $nuevo = $db
                ->insert('productos', $datos);

            if ($nuevo) {
                $msg['status']   = true;
                $msg['msg'] = 'Producto creado';
            } else {
                $msg['status'] = false;
                $msg['msg']    = 'Error, no se pudo crear el producto';
            }
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, el producto ya existe!';
        }

        echo json_encode($msg);
        break;
    case 'Producto-lista':
        $page       = $data['pagina'];
        $results_pg = 30;
        $adjacent   = 2;

        $nombre    = $db->escape(trim($data['nombre']));

        ($nombre == '' ? $nombre = '%' : '');

        $totalitems = $db
            ->where('nombre_p', '%' . $nombre . '%', 'LIKE')
            ->objectBuilder()->get('productos');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            require_once 'Paginacion.php';

            $content       = '';
            $db->pageLimit = $results_pg;

            $listing = $db
                ->where('nombre_p', '%' . $nombre . '%', 'LIKE')
                ->orderBy('Id_p', 'DESC')
                ->objectBuilder()->paginate('productos', $page);

            foreach ($listing as $producto) {
                $categoria = '';
                $marca = '';
                $estado = '';


                $categorias = $db
                    ->where('Id_c', $producto->categoria_p)
                    ->objectBuilder()->get('categorias');


                if ($db->count > 0) {
                    $categoria = $categorias[0]->nombre_c;
                }

                $marcas = $db
                    ->where('Id_m', $producto->marca_p)
                    ->objectBuilder()->get('marcas');

                if ($db->count > 0) {
                    $marca = $marcas[0]->nombre_m;
                }

                switch ($producto->estado_p) {
                    case '0':
                        $estado = 'Inactivo';
                        break;
                    case '1':
                        $estado = 'Activo';
                        break;
                }

                $content .= '<tr id="P-' . $producto->Id_p . '">
                                <td>' . $producto->Id_p . '</td>
                                <td>' . $categoria . '</td>
                                <td>' . $marca . '</td>
                                <td>' . $producto->nombre_p . '</td>
                                <td><a href="../dist/' . $producto->imagen_p . '" target="_blank" class="waves-effect waves-light btn light-blue teal darken-1">Ver</a></td>
                                <td>' . $estado . '</td>
                                <td> <a href="#!" class="waves-effect waves-light btn light-blue darken-2 Editar-producto">Editar</a></td>
                                <td> <a href="#!" class="waves-effect waves-light btn grey lighten-1 Eliminar-producto">Eliminar</a></td>
                            </tr>';
            }

            $msg['list']       = $content;
            $pagconfig         = array('pagina' => $page, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpgs, 'resultados_pag' => $results_pg, 'adyacentes' => $adjacent);
            $paginate          = new Paginacion($pagconfig);
            $msg['pagination'] = $paginate->crearlinks();
        } else {
            $msg['list'] = '<tr>
                                <td colspan="6">No hay registros</td>
                            </tr>';
            $msg['pagination'] = '';
        }

        echo json_encode($msg);
        break;
    case 'Producto-info':
        $idproducto = explode('-', $data['idproducto']);

        $check = $db
            ->where('Id_p', $idproducto[1])
            ->objectBuilder()->get('productos');

        if ($db->count > 0) {
            $productos = $db
                ->where('Id_p', $idproducto[1])
                ->objectBuilder()->get('productos');

            $msg['status'] = true;
            $msg['info']   = $productos;
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, el producto no existe!';
        }

        echo json_encode($msg);
        break;
    case 'Producto-editar':
        $idproducto = explode('-', $data['idproducto']);

        $check = $db
            ->where('nombre_p', $data['nombre'])
            ->where('Id_p', $idproducto[1], '!=')
            ->objectBuilder()->get('productos');

        if ($db->count == 0) {
            $datos = [
                'nombre_p' => $data['nombre'],
                'marca_p' => $data['marca'],
                'estado_p' => $data['estado'],
                'descripcion_p' => $data['describe'],
                'categoria_p' => $data['categoria'],
            ];

            /* if (isset($_FILES['imagen'])) {
                $ext = pathinfo($_FILES['imagen']['name'], PATHINFO_EXTENSION);
                $nombre_adjunto = limpiar(basename($_FILES['imagen']['name'], "." . $ext)) . '.' . $ext;
                $tmp_adjunto    = $_FILES['imagen']['tmp_name'];
                $archivador = $folder . date('YmdHis') . '_' . $nombre_adjunto;

                if (move_uploaded_file($tmp_adjunto, $archivador)) {
                    $productos = $db
                        ->where('Id_p', $idproducto[1])
                        ->objectBuilder()->get('productos');

                    if ($productos[0]->imagen_p != '') {
                        if (file_exists('../' . $productos[0]->imagen_p)) {
                            unlink('../' . $productos[0]->imagen_p);
                        }
                    }

                    $datos['imagen_p'] = substr($archivador, 3);
                }
            } */

            if (isset($data['imagen'])) {
                $img        = explode(',', $data['imagen']);
                $img        = base64_decode($img[1]);
                $nombre     = date('YmdHis') . Limpiar($data['imagen_nombre']) . '.jpg';
                $archivador = $folder . $nombre;

                if (file_put_contents($archivador, $img)) {
                    require 'imagine/vendor/autoload.php';
                    $imagine = new Imagine\Gd\Imagine();

                    $img = $imagine->open($archivador);
                    $img = $img->save($archivador);

                    $productos = $db
                        ->where('Id_p', $idproducto[1])
                        ->objectBuilder()->get('productos');

                    if ($productos[0]->imagen_p != '') {
                        if (file_exists('../' . $productos[0]->imagen_p)) {
                            unlink('../' . $productos[0]->imagen_p);
                        }
                    }

                    $datos['imagen_p'] = substr($archivador, 3);
                }
            }

            $editar = $db
                ->where('Id_p', $idproducto[1])
                ->update('productos', $datos);

            if ($editar) {
                $msg['status']   = true;
                $msg['msg'] = 'Producto editado';
            } else {
                $msg['status'] = false;
                $msg['msg']    = 'Error, no se pudo editar el producto';
            }
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, el producto ya existe!';
        }

        echo json_encode($msg);
        break;
    case 'Producto-eliminar':
        $idproducto = explode('-', $data['idproducto']);

        $check = $db
            ->where('Id_p', $idproducto[1])
            ->objectBuilder()->get('productos');

        if ($db->count > 0) {
            if ($check[0]->imagen_p != '') {
                if (file_exists('../' . $check[0]->imagen_p)) {
                    unlink('../' . $check[0]->imagen_p);
                }
            }

            $productos = $db
                ->where('Id_p', $idproducto[1])
                ->delete('productos');

            $msg['status'] = true;
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, el producto no existe!';
        }

        echo json_encode($msg);
        break;
}


function limpiar($String)
{
    $String = preg_replace("/[^A-Za-z0-9\_\-]/", '', $String);
    return $String;
}
