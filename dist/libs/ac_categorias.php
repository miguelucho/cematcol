<?php
require_once "conexion.php";

$data   = $_REQUEST['categoria'];
$msg    = [];

switch ($data['action']) {
    case 'Categoria-nuevo':
        $check = $db
            ->where('nombre_c', $data['nombre'])
            ->objectBuilder()->get('categorias');

        if ($db->count == 0) {
            $datos = [
                'nombre_c' => $data['nombre'],
                'estado_c' => $data['estado'],
            ];

            $nuevo = $db
                ->insert('categorias', $datos);

            if ($nuevo) {
                $msg['status']   = true;
                $msg['msg'] = 'Categoría creada';
            } else {
                $msg['status'] = false;
                $msg['msg']    = 'Error, no se pudo crear la categoría';
            }
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, la categoría ya existe!';
        }

        echo json_encode($msg);
        break;
    case 'Categoria-lista':
        $page       = $data['pagina'];
        $results_pg = 30;
        $adjacent   = 2;

        $nombre    = $db->escape(trim($data['nombre']));

        ($nombre == '' ? $nombre = '%' : '');

        $totalitems = $db
            ->where('nombre_c', '%' . $nombre . '%', 'LIKE')
            ->objectBuilder()->get('categorias');

        $numpgs = ceil($db->count / $results_pg);

        if ($numpgs >= 1) {
            require_once 'Paginacion.php';

            $content       = '';
            $db->pageLimit = $results_pg;

            $listing = $db
                ->where('nombre_c', '%' . $nombre . '%', 'LIKE')
                ->orderBy('Id_c', 'DESC')
                ->objectBuilder()->paginate('categorias', $page);

            foreach ($listing as $categoria) {
                $estado = '';

                switch ($categoria->estado_c) {
                    case '0':
                        $estado = 'Inactivo';
                        break;
                    case '1':
                        $estado = 'Activo';
                        break;
                }

                $content .= '<tr id="M-' . $categoria->Id_c . '">
                                <td>' . $categoria->Id_c . '</td>
                                <td><input type="number" step="1" min="1" class="orden" style="width:60px" value="' . $categoria->posicion_c . '"></td>
                                <td>' . $categoria->nombre_c . '</td>
                                <td>' . $estado . '</td>
                                <td> <a href="#!" class="waves-effect waves-light btn light-blue darken-2 Editar-categoria">Editar</a></td>
                                <td> <a href="#!" class="waves-effect waves-light btn grey lighten-1 Eliminar-categoria">Eliminar</a></td>
                            </tr>';
            }

            $msg['list']       = $content;
            $pagconfig         = array('pagina' => $page, 'totalrows' => $db->totalPages, 'ultima_pag' => $numpgs, 'resultados_pag' => $results_pg, 'adyacentes' => $adjacent);
            $paginate          = new Paginacion($pagconfig);
            $msg['pagination'] = $paginate->crearlinks();
        } else {
            $msg['list'] = '<tr>
                                <td colspan="6">No hay registros</td>
                            </tr>';
            $msg['pagination'] = '';
        }

        echo json_encode($msg);
        break;
    case 'Categoria-info':
        $idcategoria = explode('-', $data['idcategoria']);

        $check = $db
            ->where('Id_c', $idcategoria[1])
            ->objectBuilder()->get('categorias');

        if ($db->count > 0) {
            $marcas = $db
                ->where('Id_c', $idcategoria[1])
                ->objectBuilder()->get('categorias');

            $msg['status'] = true;
            $msg['info']   = $marcas;
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, la categoría no existe!';
        }

        echo json_encode($msg);
        break;
    case 'Categoria-editar':
        $idcategoria = explode('-', $data['idcategoria']);

        $check = $db
            ->where('nombre_c', $data['nombre'])
            ->where('Id_c', $idcategoria[1], '!=')
            ->objectBuilder()->get('categorias');

        if ($db->count == 0) {
            $datos = [
                'nombre_c' => $data['nombre'],
                'estado_c' => $data['estado'],
            ];

            $editar = $db
                ->where('Id_c', $idcategoria[1])
                ->update('categorias', $datos);

            if ($editar) {
                $msg['status']   = true;
                $msg['msg'] = 'Categoría editada';
            } else {
                $msg['status'] = false;
                $msg['msg']    = 'Error, no se pudo editar la categoría';
            }
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, la categoría ya existe!';
        }

        echo json_encode($msg);
        break;
    case 'Categoria-eliminar':
        $idcategoria = explode('-', $data['idcategoria']);

        $check = $db
            ->where('Id_c', $idcategoria[1])
            ->objectBuilder()->get('categorias');

        if ($db->count > 0) {
            $marcas = $db
                ->where('Id_c', $idcategoria[1])
                ->delete('categorias');

            $msg['status'] = true;
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, la categoría no existe!';
        }

        echo json_encode($msg);
        break;
    case 'Categoria-posicion':
        $idcategoria = explode('-', $data['idcategoria']);

        $check = $db
            ->where('Id_c', $idcategoria[1])
            ->objectBuilder()->get('categorias');

        if ($db->count > 0) {
            $marcas = $db
                ->where('Id_c', $idcategoria[1])
                ->update('categorias', ['posicion_c' => $data['posicion']]);

            $msg['status'] = true;
            $msg['info']   = $marcas;
        } else {
            $msg['status'] = false;
            $msg['msg']    = 'Error, la categoría no existe!';
        }

        echo json_encode($msg);
        break;
}
