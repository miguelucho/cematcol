$(document).ready(function () {

	$('#login').on('submit', function (e) {
		e.preventDefault();
		loader();
		data = $(this).serializeArray();
		data.push({
			name: 'login[action]',
			value: 'user-login'
		});
		$.post('../dist/libs/ac_login', data, function (data) {
			$('#fondo').remove();
			if (data.status == true) {
				window.location.href = data.redirect;
			} else if (data.status == false) {
				$.alert({
					icon: 'fa fa-warning',
					title: 'Error',
					type: 'red',
					content: data.msg,
					boxWidth: '30%',
					useBootstrap: false,
					draggable: false,
					animation:'bottom',
					closeAnimation:'opacity',
					animateFromElement:false
				});
			}
		}, 'json');
	});

	function loader() {
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader preloader-wrapper big active">' +
			'<div class="spinner-layer">' +
			'<div class="circle-clipper left">' +
			'<div class="circle"></div>' +
			'</div><div class="gap-patch">' +
			'<div class="circle"></div>' +
			'</div><div class="circle-clipper right">' +
			'<div class="circle"></div>' +
			'</div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 100);
	}
});
