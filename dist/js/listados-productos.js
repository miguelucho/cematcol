$(document).ready(function () {
    bsq_categoria = 0;
    Idproducto = 0;
    listado(1);

    $('.Categorias-productos li').on('click', function () {
        $('.Categorias-productos li').not(this).find('a').removeClass('Filtro-activo');
        if (!$('a', this).hasClass('Filtro-activo')) {
            $('a', this).addClass('Filtro-activo');
            bsq_categoria = $(this).prop('id');
        } else {
            $('a', this).removeClass('Filtro-activo');
            bsq_categoria = 0;
        }
        Idproducto = 0;
        listado(1);
    });

    $('body').on('click', '.Ver-mas', function () {
        loader();
        $.post('dist/libs/ac_listados', {
            'producto[action]': 'Producto-info',
            'producto[producto]': $(this).attr('data-producto'),
        }, function (data) {
            $('#fondo').remove();
            $.dialog({
                title: '',
                content: '<div class="Conten-modal-completo">' +
                    '<div class="Conten-modal-completo-sec">' +
                    '<img src="dist/' + data.info[0].imagen + '">' +
                    '</div>' +
                    '<div class="Conten-modal-completo-sec">' +
                    '<h4>' + data.info[0].nombre + '</h4>' +
                    data.info[0].descripcion +
                    '</div>' +
                    '</div>',
                boxWidth: '80%',
                useBootstrap: false,
                draggable: false,
                animation: 'bottom',
                closeAnimation: 'opacity',
                animateFromElement: false
            });
        }, 'json');
    });

    function listado(pg) {
        loader();
        var Idproducto = getUrlParameter('bsq');
        $('.listado-loader').fadeIn(0);
        $('.listado-productos').empty().fadeOut(0);
        $.post('dist/libs/ac_listados', {
            'producto[action]': 'Productos-lista',
            'producto[categoria]': bsq_categoria,
            'producto[producto]': Idproducto,
            'producto[pagina]': pg
        }, function (data) {
            $('#fondo').remove();
            $('.listado-productos').html(data.list);
            $('.listado-productos').fadeIn();
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
        }, 'json');
    }

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

    function loader() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader preloader-wrapper big active">' +
            '<div class="spinner-layer">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div><div class="gap-patch">' +
            '<div class="circle"></div>' +
            '</div><div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 100);
    }


    var options = {
        url: function (phrase) {
            return "dist/libs/ac_listados?producto[action]=Productos-busqueda&producto[bsq]=" + phrase + "&format=json";
        },
        getValue: "nombre",
        list: {
            match: {
                enabled: true
            },
            onChooseEvent: function () {
                $('<form action="productos" method="GET"><input name="bsq" value="' + 'Pr-' + $("#Buscador").getSelectedItemData().ID + '-Nm-' + $("#Buscador").getSelectedItemData().nombre + '"></form>').appendTo('body').submit();
            }
        }
    };

    $('#Buscador').easyAutocomplete(options);

    $('#Buscador').on('keyup', function () {
        if ($(this).val().trim() == '') {
            Idproducto = 0;
            listado(1);
        }
    });

    function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
        return 0;
    }

});
