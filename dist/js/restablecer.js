$(document).ready(function () {

	$('#recuperar-login').on('submit', function (e) {
		e.preventDefault();
		loader();
		data = $(this).serializeArray();
		data.push({
			name: 'login[action]',
			value: 'reset-pass'
		});
		$.post('../dist/libs/ac_login', data, function (data) {
			$('#fondo').remove();
			if (data.status == true) {
				notificacion('correcto', data.msg);
			} else if (data.status == false) {
				notificacion('error', data.msg);
			}
		}, 'json');
	});

	function loader() {
		$('#fondo').remove();
		$('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
		$('#fondo').append('<div class="loader preloader-wrapper big active">' +
			'<div class="spinner-layer">' +
			'<div class="circle-clipper left">' +
			'<div class="circle"></div>' +
			'</div><div class="gap-patch">' +
			'<div class="circle"></div>' +
			'</div><div class="circle-clipper right">' +
			'<div class="circle"></div>' +
			'</div>' +
			'</div>' +
			'</div>');
		setTimeout(function () {
			$('#fondo').fadeIn('fast');
		}, 100);
	}

	function notificacion(type, msg) {
		switch (type) {
			case 'error':
				color = 'red';
				icono = 'fa fa-warning';
				titulo = 'Error';
				break;
			case 'correcto':
				color = 'green';
				icono = 'fa fa-check';
				titulo = 'Correcto';
				break;
		}

		$.alert({
			icon: icono,
			title: titulo,
			type: color,
			content: msg,
			boxWidth: '30%',
			useBootstrap: false,
			draggable: false,
			animation: 'bottom',
			closeAnimation: 'opacity',
			animateFromElement: false
		});
	}
});
