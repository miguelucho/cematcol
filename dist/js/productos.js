$(document).ready(function () {
    data = new FormData();
    bsq_nombre = '';
    var $uploadCrop,
        tempFilename,
        rawImg;


    listado(1);

    $('.imagen-adjunta').on('change', function () {
        if ($(this).val() !== '') {
            if (/^image\/\w+/.test(this.files[0].type)) {
                tempFilename = $(this).val().replace(/C:\\fakepath\\/i, '');
                tempFilename = tempFilename.substr(0, tempFilename.lastIndexOf('.')) || tempFilename;
                readFile(this);
                modal({
                    type: 'info',
                    title: 'Recortar Imagen',
                    text: '<div class="modal-body">' +
                        '<div id="upload-demo" class="center-block"></div>' +
                        '</div>',
                    size: 'large',
                    callback: function (result) {
                        if (result === true) {
                            $uploadCrop.croppie('result', {
                                type: 'base64',
                                format: 'jpeg',
                                size: 'original',
                                size: {
                                    width: 225,
                                    height: 225
                                },
                            }).then(function (resp) {
                                data.append('producto[imagen]', resp);
                                data.append('producto[imagen_nombre]', tempFilename);
                            });
                        }
                    },
                    onShow: function (result) {
                        $uploadCrop = $('#upload-demo').croppie({
                            viewport: {
                                width: 225,
                                height: 225,
                            },
                            enforceBoundary: false,
                            enableExif: true,
                            enableResize: false
                        });
                        $uploadCrop.croppie('bind', {
                            url: rawImg
                        }).then(function () {
                            console.log('jQuery bind complete');
                        });
                    },
                    closeClick: false,
                    animate: true,
                    buttonText: {
                        yes: 'Confirmar',
                        cancel: 'Cancelar'
                    }
                });
            } else {
                notificacion('error', 'No es un formato válido');
                $(this).val('');
            }
        }
    });

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                rawImg = e.target.result;
            };
            reader.readAsDataURL(input.files[0]);
        } else {
            notificacion('error', 'Su navegador no soporta FileReader API');
        }
    }

    $('#Producto-nuevo').on('submit', function (e) {
        e.preventDefault();
        loader();

        otradata = $(this).serializeArray();
        data.append('producto[action]', 'Producto-nuevo');

        $.each(otradata, function (key, input) {
            data.append(input.name, input.value);
        });

        /* imagen = document.getElementById('imagen-adjunta');
        var file = imagen.files[0];
        data.append('imagen', file); */

        $.ajax({
            url: '../dist/libs/ac_productos',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                $('#fondo').remove();
                if (data.status == true) {
                    notificacion('correcto', data.msg);
                    $('input', '#Producto-nuevo').not('[type=submit]').val('');
                    $('textarea', '#Producto-nuevo').val('');
                    $("select").formSelect();
                    window.M.updateTextFields();
                    data = new FormData();
                    listado(1);
                } else {
                    notificacion('error', data.msg);
                }
            }
        });
    });

    $('body').on('click', '.Editar-producto', function () {
        loader();
        producto_editar = $(this).closest('tr').prop('id');
        $.post('../dist/libs/ac_productos', {
            'producto[action]': 'Producto-info',
            'producto[idproducto]': producto_editar
        }, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                $.each(data.info, function (i, dat) {
                    $('#Nombre-ed').val(dat.nombre_p);
                    $('#Categoria-ed').val(dat.categoria_p);
                    $('#Marca-ed').val(dat.marca_p);
                    $('#Estado-ed').val(dat.estado_p);
                    $('#Descripcion-ed').val(dat.descripcion_p);
                    window.M.updateTextFields();
                    M.FormSelect.init(document.querySelectorAll("select"));
                });
                $('#editarproducto').modal('open');
            } else if (data.status == false) {
                notificacion('error', data.msg);
            }
        }, 'json');
    });

    $('#Producto-editar').on('submit', function (e) {
        e.preventDefault();
        if (producto_editar != '') {
            loader();
            // data = new FormData();
            otradata = $(this).serializeArray();
            data.append('producto[action]', 'Producto-editar');
            data.append('producto[idproducto]', producto_editar);

            $.each(otradata, function (key, input) {
                data.append(input.name, input.value);
            });

            /* imagen = document.getElementById('imagen-adjunta-ed');
            var file = imagen.files[0];
            data.append('imagen', file); */

            $.ajax({
                url: '../dist/libs/ac_productos',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    $('#fondo').remove();
                    if (data.status == true) {
                        $('#editarproducto').modal('close');
                        $('input', '#Producto-editar').not('[type=submit]').val('');
                        $('textarea', '#Producto-editar').val('');
                        M.updateTextFields();
                        $("select option[value='']", '#Producto-editar').attr('selected', true);
                        producto_editar = '';
                        listado(1);
                        notificacion('correcto', data.msg);
                        data = new FormData();
                    } else if (data.status == false) {
                        notificacion('error', data.msg);
                    }
                }
            });
        } else {
            notificacion('error', 'Se ha producido un error!');
        }
    });

    $('body').on('click', '.Eliminar-producto', function () {
        producto_eliminar = $(this).closest('tr').prop('id');
        $.confirm({
            title: 'Eliminar Producto',
            content: 'Desea eliminar este producto?',
            buttons: {
                confirmar: {
                    btnClass: 'btn-red',
                    action: function () {
                        loader();
                        $.post('../dist/libs/ac_productos', {
                            'producto[action]': 'Producto-eliminar',
                            'producto[idproducto]': producto_eliminar,
                        }, function (data) {
                            $('#fondo').remove();
                            if (data.status == true) {
                                listado(1);
                            } else if (data.status == false) {
                                notificacion('error', data.msg);
                            }
                        }, 'json');
                    },
                },
                cancelar: function () {

                },
            },
            boxWidth: '30%',
            useBootstrap: false,
            draggable: false,
            animation: 'bottom',
            closeAnimation: 'opacity',
            animateFromElement: false
        });

    });

    $('#busqueda').on('click', function () {
        bsq_nombre = $('#buscar-nombre').val();
        listado(1);
    });

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#listado-productos').empty().fadeOut(0);
        $.post('../dist/libs/ac_productos', {
            'producto[action]': 'Producto-lista',
            'producto[nombre]': bsq_nombre,
            'producto[pagina]': pg
        }, function (data) {
            $('#listado-productos').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
            $('#listado-productos').fadeIn();
        }, 'json');
    }

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

    function loader() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader preloader-wrapper big active">' +
            '<div class="spinner-layer">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div><div class="gap-patch">' +
            '<div class="circle"></div>' +
            '</div><div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 100);
    }

    function notificacion(type, msg) {
        switch (type) {
            case 'error':
                color = 'red';
                icono = 'fa fa-warning';
                titulo = 'Error';
                break;
            case 'correcto':
                color = 'green';
                icono = 'fa fa-check';
                titulo = 'Correcto';
                break;
        }

        $.alert({
            icon: icono,
            title: titulo,
            type: color,
            content: msg,
            boxWidth: '30%',
            useBootstrap: false,
            draggable: false,
            animation: 'bottom',
            closeAnimation: 'opacity',
            animateFromElement: false
        });
    }


});
