$(document).ready(function () {
    var total_imagenes = $('#imagenes-total').val();

    data = new FormData();

    var $uploadCrop,
        tempFilename,
        rawImg;

    listado(1);


    $('#crearslide').modal({
        onOpenEnd: function () {
            if (total_imagenes >= 5) {
                notificacion('error', 'El limite máximo de imagenes es 5.');
                $('#crearslide').modal('close');
            }
        }
    });

    $('.imagen-adjunta').on('change', function () {
        if ($(this).val() !== '') {
            if (/^image\/\w+/.test(this.files[0].type)) {
                tempFilename = $(this).val().replace(/C:\\fakepath\\/i, '');
                tempFilename = tempFilename.substr(0, tempFilename.lastIndexOf('.')) || tempFilename;
                readFile(this);
                modal({
                    type: 'info',
                    title: 'Recortar Imagen',
                    text: '<div class="modal-body">' +
                        '<div id="upload-demo" class="center-block"></div>' +
                        '</div>',
                    size: 'large',
                    callback: function (result) {
                        if (result === true) {
                            $uploadCrop.croppie('result', {
                                type: 'base64',
                                format: 'jpeg',
                                // size: 'original',
                                size: {
                                    width: 1323,
                                    height: 340
                                },
                                quality: 1
                            }).then(function (resp) {
                                data.append('slider[imagen]', resp);
                                data.append('slider[imagen_nombre]', tempFilename);
                            });
                        }
                    },
                    onShow: function (result) {
                        $uploadCrop = $('#upload-demo').croppie({
                            viewport: {
                                width: 1323,
                                height: 340,
                            },
                            enforceBoundary: false,
                            enableExif: true,
                            // enableResize: false
                        });
                        $uploadCrop.croppie('bind', {
                            url: rawImg
                        }).then(function () {
                            console.log('jQuery bind complete');
                        });
                    },
                    closeClick: false,
                    animate: true,
                    buttonText: {
                        yes: 'Confirmar',
                        cancel: 'Cancelar'
                    }
                });
            } else {
                notificacion('error', 'No es un formato válido');
                $(this).val('');
            }
        }
    });

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                rawImg = e.target.result;
            };
            reader.readAsDataURL(input.files[0]);
        } else {
            notificacion('error', 'Su navegador no soporta FileReader API');
        }
    }

    $('#Slider-nuevo').on('submit', function (e) {
        e.preventDefault();
        loader();

        if (total_imagenes >= 5) {
            notificacion('error', 'El limite máximo de imagenes es 5.');
            $('#crearslide').modal('close');
        }

        otradata = $(this).serializeArray();
        data.append('slider[action]', 'Slider-nuevo');

        $.each(otradata, function (key, input) {
            data.append(input.name, input.value);
        });

        $.ajax({
            url: '../dist/libs/ac_slider',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                $('#fondo').remove();
                if (data.status == true) {
                    notificacion('correcto', data.msg);
                    $('input', '#Slider-nuevo').not('[type=submit]').val('');
                    $("select").formSelect();
                    window.M.updateTextFields();
                    total_imagenes = data.total;
                    data = new FormData();
                    listado(1);
                } else {
                    notificacion('error', data.msg);
                }
            }
        });
    });

    $('body').on('click', '.Editar-imagen', function () {
        loader();
        imagen_editar = $(this).closest('tr').prop('id');
        $.post('../dist/libs/ac_slider', {
            'slider[action]': 'Imagen-info',
            'slider[idimagen]': imagen_editar
        }, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                $.each(data.info, function (i, dat) {
                    $('#Nombre-ed').val(dat.nombre_s);
                    $('#Estado-ed').val(dat.estado_s);
                    window.M.updateTextFields();
                    M.FormSelect.init(document.querySelectorAll("select"));
                });
                $('#editarslide').modal('open');
            } else if (data.status == false) {
                notificacion('error', data.msg);
            }
        }, 'json');
    });

    $('#Slider-editar').on('submit', function (e) {
        e.preventDefault();
        if (imagen_editar != '') {
            loader();
            otradata = $(this).serializeArray();
            data.append('slider[action]', 'Slider-editar');
            data.append('slider[idimagen]', imagen_editar);

            $.each(otradata, function (key, input) {
                data.append(input.name, input.value);
            });

            $.ajax({
                url: '../dist/libs/ac_slider',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    $('#fondo').remove();
                    if (data.status == true) {
                        $('#editarslide').modal('close');
                        $('input', '#Slider-editar').not('[type=submit]').val('');
                        M.updateTextFields();
                        $("select option[value='']", '#Slider-editar').attr('selected', true);
                        imagen_editar = '';
                        listado(1);
                        notificacion('correcto', data.msg);
                        data = new FormData();
                    } else if (data.status == false) {
                        notificacion('error', data.msg);
                    }
                }
            });
        } else {
            notificacion('error', 'Se ha producido un error!');
        }
    });

    $('body').on('click', '.Eliminar-imagen', function () {
        imagen_eliminar = $(this).closest('tr').prop('id');
        $.confirm({
            title: 'Eliminar Imagen',
            content: 'Desea eliminar esta imagen?',
            buttons: {
                confirmar: {
                    btnClass: 'btn-red',
                    action: function () {
                        loader();
                        $.post('../dist/libs/ac_slider', {
                            'slider[action]': 'Slider-eliminar',
                            'slider[idimagen]': imagen_eliminar,
                        }, function (data) {
                            $('#fondo').remove();
                            if (data.status == true) {
                                total_imagenes = data.total;
                                listado(1);
                            } else if (data.status == false) {
                                notificacion('error', data.msg);
                            }
                        }, 'json');
                    },
                },
                cancelar: function () {

                },
            },
            boxWidth: '30%',
            useBootstrap: false,
            draggable: false,
            animation: 'bottom',
            closeAnimation: 'opacity',
            animateFromElement: false
        });

    });

    $('body').on('blur', '.orden', function () {
        posicion = $(this);
        error = 0;
        $.each($('.orden').not($(this)), function () {
            if ($(this).val() != '') {
                if ($(this).val() == posicion.val()) {
                    notificacion('error', 'Esa posición ya se encuentra asignada');
                    error = 1;
                    posicion.val('');
                }
            }
        });

        if (error == 0) {
            $.post('../dist/libs/ac_slider', {
                'slider[action]': 'Slider-posicion',
                'slider[idimagen]': $(this).closest('tr').prop('id'),
                'slider[posicion]': posicion.val()
            }, function (data) {
                $('#fondo').remove();
                if (data.status == true) {
                } else {

                }
            }, 'json');
        }
    });

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#listado-imagenes').empty().fadeOut(0);
        $.post('../dist/libs/ac_slider', {
            'slider[action]': 'Slider-lista',
            'slider[pagina]': pg
        }, function (data) {
            $('#listado-imagenes').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
            $('#listado-imagenes').fadeIn();
        }, 'json');
    }

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

    function loader() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader preloader-wrapper big active">' +
            '<div class="spinner-layer">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div><div class="gap-patch">' +
            '<div class="circle"></div>' +
            '</div><div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 100);
    }

    function notificacion(type, msg) {
        switch (type) {
            case 'error':
                color = 'red';
                icono = 'fa fa-warning';
                titulo = 'Error';
                break;
            case 'correcto':
                color = 'green';
                icono = 'fa fa-check';
                titulo = 'Correcto';
                break;
        }

        $.alert({
            icon: icono,
            title: titulo,
            type: color,
            content: msg,
            boxWidth: '30%',
            useBootstrap: false,
            draggable: false,
            animation: 'bottom',
            closeAnimation: 'opacity',
            animateFromElement: false
        });
    }


});
