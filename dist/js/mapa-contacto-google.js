
function initMap() {
  var colombia = new google.maps.LatLng(4.572892, -74.125891);

  var map = new google.maps.Map(document.getElementById('map'), {
    center: colombia,
    zoom: 16
  });
  
  var coordInfoWindow = new google.maps.InfoWindow({
    content: 'Cematcol, Bogotá D.C',
    position: colombia
  });

  const marker = new google.maps.Marker({
    position: colombia,
    map,
    title: "Cematcol, Bogotá D.C",
  });
  marker.addListener("click", () => {
    coordInfoWindow.open(map, marker);
  });

}

function project(latLng) {
  var siny = Math.sin(latLng.lat() * Math.PI / 180);
  // Truncating to 0.9999 effectively limits latitude to 89.189. This is
  // about a third of a tile past the edge of the world tile.
  siny = Math.min(Math.max(siny, -0.9999), 0.9999);
  return new google.maps.Point(
    TILE_SIZE * (0.5 + latLng.lng() / 360),
    TILE_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)));
}


$('#Form-contacto').on('submit', function (e) {
  e.preventDefault();
  loader2();
  data = $(this).serializeArray();

  $.post('dist/libs/ac_contacto', data, function (data) {
    $('#fondo').remove();
    if (data.status == true) {
      $('input', '#Form-contacto').not('[type=submit]').val('');
      $('textarea', '#Form-contacto').val('');
      notificacion2('correcto', data.msg);
    } else if (data.status == false) {
      notificacion2('error', data.msg);
    }
  }, 'json');
});

function loader2() {
  $('#fondo').remove();
  $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
  $('#fondo').append('<div class="loader preloader-wrapper big active">' +
    '<div class="spinner-layer">' +
    '<div class="circle-clipper left">' +
    '<div class="circle"></div>' +
    '</div><div class="gap-patch">' +
    '<div class="circle"></div>' +
    '</div><div class="circle-clipper right">' +
    '<div class="circle"></div>' +
    '</div>' +
    '</div>' +
    '</div>');
  setTimeout(function () {
    $('#fondo').fadeIn('fast');
  }, 100);
}

function notificacion2(type, msg) {
  switch (type) {
    case 'error':
      color = 'red';
      icono = 'fa fa-warning';
      titulo = 'Error';
      break;
    case 'correcto':
      color = 'green';
      icono = 'fa fa-check';
      titulo = 'Correcto';
      break;
  }

  $.alert({
    icon: icono,
    title: titulo,
    type: color,
    content: msg,
    boxWidth: '30%',
    useBootstrap: false,
    draggable: false,
    animation: 'bottom',
    closeAnimation: 'opacity',
    animateFromElement: false
  });
}
