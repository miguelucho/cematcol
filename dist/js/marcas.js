$(document).ready(function () {
    bsq_nombre = '';
    listado(1);

    $('#Marca-nueva').on('submit', function (e) {
        e.preventDefault();
        loader();
        data = $(this).serializeArray();
        data.push({
            name: 'marca[action]',
            value: 'Marca-nuevo'
        });
        $.post('../dist/libs/ac_marcas', data, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                $('input', '#Marca-nueva').not('[type=submit]').val('');
                $("select").formSelect();
                listado(1);
                notificacion('correcto', data.msg);
            } else if (data.status == false) {
                notificacion('error', data.msg);
            }
        }, 'json');
    });

    $('body').on('click', '.Editar-marca', function () {
        loader();
        marca_editar = $(this).closest('tr').prop('id');
        $.post('../dist/libs/ac_marcas', {
            'marca[action]': 'Marca-info',
            'marca[idmarca]': marca_editar
        }, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                $.each(data.info, function (i, dat) {
                    $('#Nombre-ed').val(dat.nombre_m);
                    $('#Estado-ed').val(dat.estado_m);
                    window.M.updateTextFields();
                    M.FormSelect.init(document.querySelectorAll("select"));
                });
                $('#editarmarca').modal('open');
            } else if (data.status == false) {
                notificacion('error', data.msg);
            }
        }, 'json');
    });

    $('#Marca-editar').on('submit', function (e) {
        e.preventDefault();
        if (marca_editar != '') {
            loader();
            data = $(this).serializeArray();
            data.push({
                name: 'marca[action]',
                value: 'Marca-editar'
            }, {
                name: 'marca[idmarca]',
                value: marca_editar
            });

            $.post('../dist/libs/ac_marcas', data, function (data) {
                $('#fondo').remove();
                if (data.status == true) {
                    $('#editarmarca').modal('close');
                    $('input', '#Marca-editar').not('[type=submit]').val('');
                    M.updateTextFields();
                    $("select option[value='']", '#Marca-editar').attr('selected', true);
                    marca_editar = '';
                    listado(1);
                    notificacion('correcto', data.msg);
                } else if (data.status == false) {
                    notificacion('error', data.msg);
                }
            }, 'json');
        } else {
            notificacion('error', 'Se ha producido un error!');
        }
    });

    $('body').on('click', '.Eliminar-marca', function () {
        marca_eliminar = $(this).closest('tr').prop('id');
        $.confirm({
            title: 'Eliminar Marca',
            content: 'Desea eliminar esta marca?',
            buttons: {
                confirmar: {
                    btnClass: 'btn-red',
                    action: function () {
                        loader();
                        $.post('../dist/libs/ac_marcas', {
                            'marca[action]': 'Marca-eliminar',
                            'marca[idmarca]': marca_eliminar,
                        }, function (data) {
                            $('#fondo').remove();
                            if (data.status == true) {
                                listado(1);
                            } else if (data.status == false) {
                                notificacion('error', data.msg);
                            }
                        }, 'json');
                    },
                },
                cancelar: function () {

                },
            },
            boxWidth: '30%',
            useBootstrap: false,
            draggable: false,
            animation: 'bottom',
            closeAnimation: 'opacity',
            animateFromElement: false
        });

    });


    $('body').on('blur', '.orden', function () {
        posicion = $(this);
        error = 0;
        $.each($('.orden').not($(this)), function () {
            if ($(this).val() != '') {
                if ($(this).val() == posicion.val()) {
                    notificacion('error', 'Esa posición ya se encuentra asignada');
                    error = 1;
                    posicion.val('');
                }
            }
        });

        if (error == 0) {
            $.post('../dist/libs/ac_marcas', {
                'marca[action]': 'Marca-posicion',
                'marca[idmarca]': $(this).closest('tr').prop('id'),
                'marca[posicion]': posicion.val()
            }, function (data) {
                $('#fondo').remove();
                if (data.status == true) {
                } else {
                    nmensaje('error', 'Error', data.motivo);
                }
            }, 'json');
        }
    });

    $('#busqueda').on('click', function () {
        bsq_nombre = $('#buscar-nombre').val();
        listado(1);
    });

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#listado-marcas').empty().fadeOut(0);
        $.post('../dist/libs/ac_marcas', {
            'marca[action]': 'Marca-lista',
            'marca[nombre]': bsq_nombre,
            'marca[pagina]': pg
        }, function (data) {
            $('#listado-marcas').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
            $('#listado-marcas').fadeIn();
        }, 'json');
    }

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

    function loader() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader preloader-wrapper big active">' +
            '<div class="spinner-layer">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div><div class="gap-patch">' +
            '<div class="circle"></div>' +
            '</div><div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 100);
    }

    function notificacion(type, msg) {
        switch (type) {
            case 'error':
                color = 'red';
                icono = 'fa fa-warning';
                titulo = 'Error';
                break;
            case 'correcto':
                color = 'green';
                icono = 'fa fa-check';
                titulo = 'Correcto';
                break;
        }

        $.alert({
            icon: icono,
            title: titulo,
            type: color,
            content: msg,
            boxWidth: '30%',
            useBootstrap: false,
            draggable: false,
            animation: 'bottom',
            closeAnimation: 'opacity',
            animateFromElement: false
        });
    }


});
