$(document).ready(function () {
    bsq_nombre = '';
    listado(1);

    $('#Usuario-nuevo').on('submit', function (e) {
        e.preventDefault();
        loader();

        data = $(this).serializeArray();
        data.push({
            name: 'usuario[action]',
            value: 'Usuario-nuevo'
        });

        $.post('../dist/libs/ac_usuarios', data, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                notificacion('correcto', data.msg);
                $('input', '#Usuario-nuevo').not('[type=submit]').val('');
                $("select").formSelect();
                window.M.updateTextFields();
                listado(1);
            } else {
                notificacion('error', data.msg);
            }
        }, 'json');
    });


    $('body').on('click', '.Editar-usuario', function () {
        loader();
        usuario_editar = $(this).closest('tr').prop('id');
        $.post('../dist/libs/ac_usuarios', {
            'usuario[action]': 'Usuario-info',
            'usuario[idusuario]': usuario_editar
        }, function (data) {
            $('#fondo').remove();
            if (data.status == true) {
                $.each(data.info, function (i, dat) {
                    $('#Nombre-ed').val(dat.nombre_us);
                    $('#Apellido-ed').val(dat.apellido_us);
                    $('#Identificacion-ed').val(dat.identificacion_us);
                    $('#Correo-ed').val(dat.correo_us);
                    $('#Estado-ed').val(dat.estado_us);
                    window.M.updateTextFields();
                    M.FormSelect.init(document.querySelectorAll("select"));
                });
                $('#editarusuario').modal('open');
            } else if (data.status == false) {
                notificacion('error', data.msg);
            }
        }, 'json');
    });

    $('#Usuario-editar').on('submit', function (e) {
        e.preventDefault();
        if (usuario_editar != '') {
            loader();

            data = $(this).serializeArray();
            data.push({
                name: 'usuario[action]',
                value: 'Usuario-editar'
            }, {
                name: 'usuario[idusuario]',
                value: usuario_editar
            });

            $.post('../dist/libs/ac_usuarios', data, function (data) {
                $('#fondo').remove();
                if (data.status == true) {
                    $('#editarusuario').modal('close');
                    $('input', '#Usuario-editar').not('[type=submit]').val('');
                    $("select option[value='']", '#Usuario-editar').attr('selected', true);
                    usuario_editar = '';
                    listado(1);
                    notificacion('correcto', data.msg);
                } else if (data.status == false) {
                    notificacion('error', data.msg);
                }
            }, 'json');
        } else {
            notificacion('error', 'Se ha producido un error!');
        }
    });

    $('body').on('click', '.Eliminar-usuario', function () {
        usuario_eliminar = $(this).closest('tr').prop('id');
        $.confirm({
            title: 'Eliminar Usuario',
            content: 'Desea eliminar este usuario?',
            buttons: {
                confirmar: {
                    btnClass: 'btn-red',
                    action: function () {
                        loader();
                        $.post('../dist/libs/ac_usuarios', {
                            'usuario[action]': 'Usuario-eliminar',
                            'usuario[idusuario]': usuario_eliminar,
                        }, function (data) {
                            $('#fondo').remove();
                            if (data.status == true) {
                                listado(1);
                            } else if (data.status == false) {
                                notificacion('error', data.msg);
                            }
                        }, 'json');
                    },
                },
                cancelar: function () {

                },
            },
            boxWidth: '30%',
            useBootstrap: false,
            draggable: false,
            animation: 'bottom',
            closeAnimation: 'opacity',
            animateFromElement: false
        });

    });

    $('#busqueda').on('click', function () {
        bsq_nombre = $('#buscar-nombre').val();
        listado(1);
    });

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#listado-usuarios').empty().fadeOut(0);
        $.post('../dist/libs/ac_usuarios', {
            'usuario[action]': 'Usuario-lista',
            'usuario[nombre]': bsq_nombre,
            'usuario[pagina]': pg
        }, function (data) {
            $('#listado-usuarios').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
            $('#listado-usuarios').fadeIn();
        }, 'json');
    }

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });

    function loader() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader preloader-wrapper big active">' +
            '<div class="spinner-layer">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div><div class="gap-patch">' +
            '<div class="circle"></div>' +
            '</div><div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 100);
    }

    function notificacion(type, msg) {
        switch (type) {
            case 'error':
                color = 'red';
                icono = 'fa fa-warning';
                titulo = 'Error';
                break;
            case 'correcto':
                color = 'green';
                icono = 'fa fa-check';
                titulo = 'Correcto';
                break;
        }

        $.alert({
            icon: icono,
            title: titulo,
            type: color,
            content: msg,
            boxWidth: '30%',
            useBootstrap: false,
            draggable: false,
            animation: 'bottom',
            closeAnimation: 'opacity',
            animateFromElement: false
        });
    }

});
