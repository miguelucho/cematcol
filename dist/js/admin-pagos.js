$(document).ready(function () {
    bsq_nombre = '';
    listado(1);

    $('#busqueda').on('click', function () {
        bsq_nombre = $('#buscar-nombre').val();
        listado(1);
    });

    function listado(pg) {
        $('.listado-loader').fadeIn(0);
        $('#listado-pagos').empty().fadeOut(0);
        $.post('../dist/libs/ac_pagos', {
            'pago[opc]': 'Pagos-lista',
            'pago[nombre]': bsq_nombre,
            'pago[pagina]': pg
        }, function (data) {
            $('#listado-pagos').html(data.list);
            $('.listado-paginacion ul').html(data.pagination);
            $('.listado-loader').fadeOut(0);
            $('#listado-pagos').fadeIn();
        }, 'json');
    }

    $('body').on('click', '.mpag', function () {
        listado($(this).prop('id'));
    });
});
