$(document).ready(function () {
    var scroll = new SmoothScroll('a[href*="#"]', {

        // Selectors
        ignore: '[data-scroll-ignore]', // Selector for links to ignore (must be a valid CSS selector)
        header: null, // Selector for fixed headers (must be a valid CSS selector)
        topOnEmptyHash: true, // Scroll to the top of the page for links with href="#"

        // Speed & Duration
        speed: 500, // Integer. Amount of time in milliseconds it should take to scroll 1000px
        speedAsDuration: false, // If true, use speed as the total duration of the scroll animation
        durationMax: null, // Integer. The maximum amount of time the scroll animation should take
        durationMin: null, // Integer. The minimum amount of time the scroll animation should take
        clip: true, // If true, adjust scroll distance to prevent abrupt stops near the bottom of the page
        offset: 160,


        easing: 'easeInOutCubic', // Easing pattern to use
        customEasing: function (time) {


            return time < 0.5 ? 2 * time * time : -1 + (4 - 2 * time) * time;

        },

        // History
        updateURL: true, // Update the URL on scroll
        popstate: true, // Animate scrolling with the forward/backward browser buttons (requires updateURL to be true)

        // Custom Events
        emitEvents: true // Emit custom events

    });

    AOS.init();
    /*  $(function () {
         $('#slides').superslides({
             inherit_width_from: '.wide-container',
             inherit_height_from: '.wide-container',
             play: 3000,
             animation: 'fade'
         });
     }); */

    $('#slides').slick({
        autoplay: true,
        dots: true,
        fade: true,
        pauseOnFocus: false,
        pauseOnHover: false,
        arrows: false,
    });


    let video = document.querySelector('#Estadist');

    let observer = new IntersectionObserver((entries, observer) => {
        entries.forEach(entry => {
            var counter = {
                var: 0
            };
            var nombre = TweenMax.to(counter, 1, {
                var: 5,
                onUpdate: function () {
                    var number = Math.ceil(counter.var);
                    $('.counter').html(number);
                    if (number === counter.var) {
                        nombre.kill();
                    }
                },
                onComplete: function () { },
                ease: Circ.easeOut
            });

            var counter2 = {
                var: 0
            };
            var nombre = TweenMax.to(counter2, 20, {
                var: 600,
                onUpdate: function () {
                    var number = Math.ceil(counter2.var);
                    $('.counter2').html(number);
                    if (number === counter2.var) {
                        nombre.kill();
                    }
                },
                onComplete: function () { },
                ease: Circ.easeOut
            });

            var counter3 = {
                var: 0
            };
            var nombre = TweenMax.to(counter3, 1, {
                var: 1,
                onUpdate: function () {
                    var number = Math.ceil(counter3.var);
                    $('.counter3').html(number);
                    if (number === counter3.var) {
                        nombre.kill();
                    }
                },
                onComplete: function () { },
                ease: Circ.easeOut
            });

            var counter4 = {
                var: 0
            };
            var nombre = TweenMax.to(counter4, 3, {
                var: 150,
                onUpdate: function () {
                    var number = Math.ceil(counter4.var);
                    $('.counter4').html(number);
                    if (number === counter4.var) {
                        nombre.kill();
                    }
                },
                onComplete: function () { },
                ease: Circ.easeOut
            });

            var counter5 = {
                var: 0
            };
            var nombre = TweenMax.to(counter5, 1, {
                var: 300,
                onUpdate: function () {
                    var number = Math.ceil(counter5.var);
                    $('.counter5').html(number);
                    if (number === counter5.var) {
                        nombre.kill();
                    }
                },
                onComplete: function () { },
                ease: Circ.easeOut
            });


        });
    }, {
        threshold: 1
    });
    observer.observe(video);

    $('body').on('click', '.Ver-mas', function () {
        loader();
        $.post('dist/libs/ac_listados', {
            'producto[action]': 'Producto-info',
            'producto[producto]': $(this).attr('data-producto'),
        }, function (data) {
            $('#fondo').remove();
            $.dialog({
                title: '',
                content: '<div class="Conten-modal-completo">' +
                    '<div class="Conten-modal-completo-sec">' +
                    '<img src="dist/' + data.info[0].imagen + '">' +
                    '</div>' +
                    '<div class="Conten-modal-completo-sec">' +
                    '<h4>' + data.info[0].nombre + '</h4>' +
                    data.info[0].descripcion +
                    '</div>' +
                    '</div>',
                boxWidth: '80%',
                useBootstrap: false,
                draggable: false,
                animation: 'bottom',
                closeAnimation: 'opacity',
                animateFromElement: false
            });
        }, 'json');
    });

    function loader() {
        $('#fondo').remove();
        $('body').append("<div class='fondo' id='fondo' style='display:none;'></div>");
        $('#fondo').append('<div class="loader preloader-wrapper big active">' +
            '<div class="spinner-layer">' +
            '<div class="circle-clipper left">' +
            '<div class="circle"></div>' +
            '</div><div class="gap-patch">' +
            '<div class="circle"></div>' +
            '</div><div class="circle-clipper right">' +
            '<div class="circle"></div>' +
            '</div>' +
            '</div>' +
            '</div>');
        setTimeout(function () {
            $('#fondo').fadeIn('fast');
        }, 100);
    }

    var options = {
        url: function (phrase) {
            return "dist/libs/ac_listados?producto[action]=Productos-busqueda&producto[bsq]=" + phrase + "&format=json";
        },
        getValue: "nombre",
        list: {
            match: {
                enabled: true
            },
            onChooseEvent: function () {
                $('<form action="productos" method="GET"><input name="bsq" value="' + 'Pr-' + $("#Buscador").getSelectedItemData().ID + '-Nm-' + $("#Buscador").getSelectedItemData().nombre + '"></form>').appendTo('body').submit();
            }
        }
    };

    $('#Buscador').easyAutocomplete(options);

});
