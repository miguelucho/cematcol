<?php
require_once "dist/libs/conexion.php";
$ls_imagenes = '';
$ls_productos = '';

$imagenes = $db
  ->ObjectBuilder()
  ->rawQuery('SELECT * FROM slider WHERE estado_s = 1 ORDER BY  CAST(posicion_s as SIGNED INTEGER) ASC ');

foreach ($imagenes as $imagen) {
  /* $ls_imagenes .= '<li>
                    <img src="dist/' . $imagen->imagen_s . '" alt="">
                    <div class="context">
                    </div>
                  </li>'; */

  $ls_imagenes .= '<img src="dist/' . $imagen->imagen_s . '" alt="">';
}

$productos = $db
  ->ObjectBuilder()
  ->rawQuery('SELECT * FROM productos WHERE estado_p = 1 ORDER BY  CAST(vistas_p as SIGNED INTEGER) DESC LIMIT 4 ');

foreach ($productos as $producto) {
  $marca = '';
  $marcas = $db
    ->where('Id_m', $producto->marca_p)
    ->objectBuilder()->get('marcas');

  if ($db->count > 0) {
    $marca = $marcas[0]->nombre_m;
  }

  $ls_productos .= '<div class="Producto-ficha" data-aos="fade-up" data-aos-delay="580" data-aos-duration="1500">
                      <div class="Producto-ficha-arriba">
                        <div class="Producto-ficha-imagen">
                          <img src="dist/' . $producto->imagen_p . '" alt="">
                        </div>
                      </div>
                      <div class="Producto-ficha-abajo">
                        <div class="Producto-ficha-descripcion">
                          <div class="Producto-ficha-nombre">
                            <span>' . $producto->nombre_p . '</span>
                          </div>
                          <div class="Producto-ficha-marca">
                            <span>' . $marca . '</span>
                          </div>
                          <div class="Producto-ficha-contenbtn">
                            <a href="#!" class="Btn-rojo-redondo Btn-block Ver-mas" data-producto="Pr-' . $producto->Id_p . '"> Saber Mas</a>
                          </div>
                        </div>
                      </div>
                    </div>';
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords" lang="es" content="">
  <meta name="robots" content="All">
  <meta name="description" lang="es" content="">
  <title>CEMATCOL | Cementos y Materiales de Colombia</title>
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" href="dist/css/load.css" />
  <link rel="stylesheet" href="dist/css/bundled.css" />
  <link rel="stylesheet" href="dist/css/easy-autocomplete.css">
  <link rel="stylesheet" href="dist/css/jquery-confirm.min.css" />
  <link rel="stylesheet" type="text/css" href="dist/slick/slick.css" />
  <link rel="stylesheet" type="text/css" href="dist/slick/slick-theme.css" />

  <style>
    .jconfirm.jconfirm-white .jconfirm-box,
    .jconfirm.jconfirm-light .jconfirm-box {
      max-width: 800px !important;
    }

    @media screen and (max-width: 600px) {
      .Banner {
        padding-top: 140px !important;
      }
    }

    @media screen and (max-width: 360px) {
      .Banner {
        padding-top: 155px !important;
      }
    }
  </style>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-QHGYVVW2HJ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-QHGYVVW2HJ');
  </script>
</head>

<body>
  <header>
    <?php include("dist/libs/pages/header-top.php") ?>
  </header>

  <div class="Banner">
    <?php include("dist/libs/pages/banner-home.php") ?>
  </div>


  <section>
    <div class="Productos-destacados">
      <div class="Productos-destacados-int">
        <div class="Productos-destacados-titulo">
          <h3>Productos Destacados</h3>
        </div>
        <div class="Productos-destacados-contenedor">
          <?php echo $ls_productos ?>
        </div>

        <div class="Productos-conten-vermas">
          <a href="productos" class="Btn-negro-redondo">Ver todos</a>
        </div>


      </div>
    </div>
  </section>

  <section id="Estadist">
    <?php include("dist/libs/pages/estadisticas.php") ?>
  </section>

  <section>
    <?php include("dist/libs/pages/marcas-aliadas.php") ?>
  </section>

  <section>
    <?php include("dist/libs/pages/contacto.php") ?>
  </section>

  <section>
    <?php include("dist/libs/pages/whatsapp.php") ?>
  </section>

  <script src="dist/js/jquery.min.js"></script>
  <script src="dist/js/menu-public.js"></script>
  <script src="dist/js/TweenMax.min.js"></script>
  <script src="dist/js/aos.js"></script>
  <!-- <script src="dist/js/jquery.superslides.min.js"></script> -->
  <script type="text/javascript" src="dist/slick/slick.min.js"></script>
  <script src="dist/js/jquery-confirm.min.js"></script>
  <script src="dist/js/mapa-contacto-google.js"></script>
  <script src="dist/js/smooth-scroll.polyfills.js"></script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfdouGws62iXdkOXVek4RmrdTQSuOJs38&callback=initMap" type="text/javascript"></script>
  <script src="dist/js/jquery.easy-autocomplete.min.js"></script>
  <script src="dist/js/index.js?v<?php echo date('YmdHis') ?>"></script>
</body>

</html>
