<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords" lang="es" content="">
  <meta name="robots" content="All">
  <meta name="description" lang="es" content="">
  <title>Políticas | CEMATCOL | Cementos y Materiales de Colombia</title>
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" href="dist/css/load.css" />
  <link rel="stylesheet" href="dist/css/bundled.css" />
  <link rel="stylesheet" href="dist/css/jquery-confirm.min.css" />
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-QHGYVVW2HJ"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-QHGYVVW2HJ');
  </script>
</head>

<body>
  <header>
    <?php include("dist/libs/pages/header-top.php") ?>
  </header>

  <section>
    <div class="Mashead">
      <div class="Mashead-int">
        <div class="Mashead-int-sombra">
          <div class="Mashead-int-texto">
            <div class="Mashead-int-texto-con">
              <h1>POLÍTICAS Y USO DE DATOS</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="Seccion-nosotros">
      <!-- <div class="Seccion-nosotros-int-doble">
        <div class="Seccion-nosotros-int-doble-sec">
          <p>Con más de 20 años de experiencia en el sector de distribución de materiales para la construcción, somos la empresa líder en el sur de Bogotá que provee los insumos de obra negra de mayor calidad a Ferreterías y Obras. </p>
          <p>
            Contamos con las mejores marcas de la industria, un equipo altamente capacitado y rápidos tiempos de entrega
          </p>
          <p>Nuestro compromiso! Aportar al desarrollo y progreso de la capital Colombiana y sus municipios aledaños.</p>
        </div>
        <div class="Seccion-nosotros-int-doble-sec">
          <img src="dist/assets/images/img_6.jpg" alt="">
        </div>
      </div> -->

      <div class="Seccion-nosotros-int-sencillo">

        <p>En CEMATCOL respetamos el derecho que tienen todas las personas a conocer,
          actualizar y rectificar las informaciones que se hayan recogido sobre ellas en bases de
          datos o archivos, así como el derecho a la información. Nuestra política de privacidad fue
          creada de conformidad con la Ley 1581 de 2012, con el propósito de informar acerca del
          tratamiento que le damos a la información que suministra en nuestro formulario de
          contacto o correo electrónico.</p>
        <p>La información suministrada a CEMATCOL es para uso interno y hace parte del proceso
          de mejora continua en cuanto a la venta y distribución de productos. CEMATCOL no
          rentará, compartirá, ni venderá información recolectada con nuestro formulario de
          contacto o por correo electrónico.</p>
        <p>Si en algún momento tiene dudas acerca de esta política podrá escribirnos
          a cematcolltda@gmail.com o en nuestro formulario de  contacto , también podrá visitar
          nuestra web www.cematcol.com</p>


        <p>1) Derechos de los titulares de la información. Usted como titular de los datos personales tendrá los siguientes derechos:</p>
        <p>
        <ol type="a">
          <li>Conocer, actualizar y rectificar sus datos personales frente a CEMATCOL. Este derecho se podrá ejercer, entre otros frente a datos parciales, inexactos, incompletos, fraccionados, que induzcan a error, o aquellos cuyo tratamiento esté expresamente prohibido o no haya sido autorizado.</li>
          <li>Solicitar prueba de la autorización otorgada a CEMATCOL salvo cuando expresamente se exceptúe como requisito para el tratamiento.</li>
          <li>Ser informado por CEMATCOL del tratamiento, previa solicitud, respecto del uso que le ha dado a sus datos personales.</li>
          <li>Presentar ante la Superintendencia de Industria y Comercio quejas por infracciones a lo dispuesto en la presente ley y las demás normas que la modifiquen, adicionen o complementen.</li>
          <li>Revocar la autorización y/o solicitar la supresión del dato cuando en el tratamiento no se respeten los principios, derechos y garantías constitucionales y legales. La revocatoria y/o supresión procederá cuando la Superintendencia de Industria y Comercio haya determinado que CEMATCOL ha incurrido en conductas contrarias a la ley y a la Constitución.</li>
          <li>Acceder en forma gratuita a sus datos personales que hayan sido objeto de tratamiento.</li>
        </ol>
        </p>
        <p>2) Autorización del titular. En el tratamiento se requiere la autorización previa e informada del Titular, la cual deberá ser obtenida por cualquier medio que pueda ser objeto de consulta posterior. En este caso al enviar el formulario de contacto a CEMATCOL usted está autorizándonos para realizar el tratamiento de sus datos bajo esta política.</p>
        <p>3) Datos sensibles. Se entiende por datos sensibles aquellos que afectan la intimidad del titular o cuyo uso indebido puede generar su discriminación, tales como aquellos que revelen el origen racial o étnico, la orientación política, las convicciones religiosas o filosóficas, la pertenencia a sindicatos, organizaciones sociales, de derechos humanos o que promueva intereses de cualquier partido político o que garanticen los derechos y garantías de partidos políticos de oposición así como los datos relativos a la salud, a la vida sexual y los datos biométricos. CEMATCOL en ningún caso solicitará este tipo de información considerada como dato sensible.</p>
        <p>4) Reclamos. Si usted como titular o sus causahabientes consideran que la información contenida en la base de datos debe ser objeto de corrección, actualización o supresión, podrán presentar un reclamo ante CEMATCOL el cual será tramitado bajo las siguientes reglas:</p>
        <p>
        <ol type="1">
          <li>El reclamo se formulará mediante solicitud dirigida a CEMATCOL, con la identificación del Titular, la descripción de los hechos que dan lugar al reclamo, la dirección, y acompañando los documentos que se quiera hacer valer. Si el reclamo resulta incompleto, se requerirá al interesado dentro de los cinco (5) días siguientes a la recepción del reclamo para que subsane las fallas. Transcurridos dos (2) meses desde la fecha del requerimiento, sin que el solicitante presente la información requerida, se entenderá que ha desistido del reclamo.

            En caso de que quien reciba el reclamo no sea competente para resolverlo, dará traslado a quien corresponda en un término máximo de dos (2) días hábiles e informará de la situación al interesado.
          </li>
          <li>Una vez recibido el reclamo completo, se incluirá en la base de datos una leyenda que diga “reclamo en trámite” y el motivo del mismo, en un término no mayor a dos (2) días hábiles. Dicha leyenda deberá mantenerse hasta que el reclamo sea decidido. </li>
          <li>El término máximo para atender el reclamo será de quince (15) días hábiles contados a partir del día siguiente a la fecha de su recibo. Cuando no fuere posible atender el reclamo dentro de dicho término, se informará al interesado los motivos de la demora y la fecha en que se atenderá su reclamo, la cual en ningún caso podrá superar los ocho (8) días hábiles siguientes al vencimiento del primer término.</li>
        </ol>
        </p>
        <p>5) Datos del Responsable del Tratamiento:</p>
        <p>Razón social: Cementos y Materiales de Colombia Ltda<br>
          NIT: 900.629.635-5<br>
          Dirección: Calle 46 Sur Nº 20- 71, Santa Lucia, Bogotá D.C.<br>
          Correo Electrónico: cematcolltda@gmail.com<br>
          Teléfono: 3124358431<br>
          Página web: www. Cematcol.com</p>









      </div>
    </div>
  </section>

  <section>
    <?php include("dist/libs/pages/marcas-aliadas.php") ?>
  </section>

  <section>
    <?php include("dist/libs/pages/contacto.php") ?>
  </section>
  <section>
    <?php include("dist/libs/pages/whatsapp.php") ?>
  </section>

  <script src="dist/js/jquery.min.js"></script>
  <script src="dist/js/menu-public.js"></script>
  <script src="dist/js/TweenMax.min.js"></script>
  <script src="dist/js/aos.js"></script>
  <script src="dist/js/jquery.superslides.min.js"></script>
  <script src="dist/js/jquery-confirm.min.js"></script>
  <script src="dist/js/mapa-contacto-google.js"></script>
  <script src="dist/js/smooth-scroll.polyfills.js"></script>
  <script>
    var scroll = new SmoothScroll('a[href*="#"]', {

      // Selectors
      ignore: '[data-scroll-ignore]', // Selector for links to ignore (must be a valid CSS selector)
      header: null, // Selector for fixed headers (must be a valid CSS selector)
      topOnEmptyHash: true, // Scroll to the top of the page for links with href="#"

      // Speed & Duration
      speed: 500, // Integer. Amount of time in milliseconds it should take to scroll 1000px
      speedAsDuration: false, // If true, use speed as the total duration of the scroll animation
      durationMax: null, // Integer. The maximum amount of time the scroll animation should take
      durationMin: null, // Integer. The minimum amount of time the scroll animation should take
      clip: true, // If true, adjust scroll distance to prevent abrupt stops near the bottom of the page
      offset: 160,


      easing: 'easeInOutCubic', // Easing pattern to use
      customEasing: function(time) {


        return time < 0.5 ? 2 * time * time : -1 + (4 - 2 * time) * time;

      },

      // History
      updateURL: true, // Update the URL on scroll
      popstate: true, // Animate scrolling with the forward/backward browser buttons (requires updateURL to be true)

      // Custom Events
      emitEvents: true // Emit custom events

    });
  </script>
  <script>
    $(document).ready(function() {
      $('#Fil-marca').on('click', function() {
        console.log('Click');
        $('.Seccion-prodmarcas-int-contenedor-sec1').toggleClass('Ocultar-filtro')

      });
    });
  </script>
  <script>
    AOS.init();
    $(function() {
      $('#slides').superslides({
        inherit_width_from: '.wide-container',
        inherit_height_from: '.wide-container',
        play: 3000,
        animation: 'fade'
      });
    });
  </script>


  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDfdouGws62iXdkOXVek4RmrdTQSuOJs38&callback=initMap" type="text/javascript"></script>
</body>

</html>